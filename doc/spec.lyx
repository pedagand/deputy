#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip smallskip
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
DepUTy : Dependent Types for You!
\begin_inset Newline newline
\end_inset

(initial specification)
\end_layout

\begin_layout Section
Introduction
\end_layout

\begin_layout Standard
DepUTy is a new experimental dependently-typed programming language embedded
 in the Clojure environment.
\end_layout

\begin_layout Standard
This document gives the basic specification of the initial revision of the
 core language.
 It is likely that the formal (or seemingly formal) specifications will
 not be kept up-to-date with the implementation, but it is a better starting
 point for understanding what should be implemented.
\end_layout

\begin_layout Standard
In this preliminary version the only available datatype is 
\begin_inset Formula $\mathbb{B}$
\end_inset

 the types of booleans 
\begin_inset Formula $\bot$
\end_inset

 (false) and 
\begin_inset Formula $\top$
\end_inset

 (true).
\end_layout

\begin_layout Section
Syntax
\end_layout

\begin_layout Standard
Unsurprisingly, the core language is a dependently-typed variant of the
 lambda-calculus.
\end_layout

\begin_layout Standard
As it is now relatively common in the dependent-type world (refs ?), terms
 are separated in two categories :
\end_layout

\begin_layout Itemize
the 
\begin_inset Quotes eld
\end_inset

neutral
\begin_inset Quotes erd
\end_inset

 terms on the one side
\end_layout

\begin_layout Itemize
the 
\begin_inset Quotes eld
\end_inset

computation
\begin_inset Quotes erd
\end_inset

 terms on the other side
\end_layout

\begin_layout Subsection*
Neutral terms
\end_layout

\begin_layout Standard
A neutral term 
\begin_inset Formula $t$
\end_inset

 (also 
\begin_inset Formula $\text{A}$
\end_inset

, 
\begin_inset Formula $B$
\end_inset

, etc.) is either:
\end_layout

\begin_layout Itemize
a lambda-abstraction: 
\begin_inset Formula $\lambda x.t$
\end_inset

 with 
\begin_inset Formula $x$
\end_inset

 a (bound) variable, and 
\begin_inset Formula $t$
\end_inset

 a proper term possibliy containing (free) occurrences of 
\begin_inset Formula $x$
\end_inset


\end_layout

\begin_layout Itemize
a type abstraction: 
\begin_inset Formula $(x:A)\rightarrow B$
\end_inset

 with 
\begin_inset Formula $x$
\end_inset

 a (bound) variable of type
\begin_inset Formula $A$
\end_inset

, and 
\begin_inset Formula $B$
\end_inset

 a proper term possibly containing (free) occurrences of 
\begin_inset Formula $x$
\end_inset

.
\end_layout

\begin_layout Itemize
a computation term: 
\begin_inset Formula $e$
\end_inset

 (cf.
 below)
\end_layout

\begin_layout Itemize
the type of types: 
\begin_inset Formula $\mathfrak{T}$
\end_inset


\end_layout

\begin_layout Itemize
the (constant) type unit 
\begin_inset Formula $\mathfrak{U}$
\end_inset

 and its only inhabitant 
\begin_inset Formula $\mathcal{U}$
\end_inset


\end_layout

\begin_layout Itemize
the (constant) type of booleans: 
\begin_inset Formula $\mathbb{B}$
\end_inset

 and the two inhabitants: 
\begin_inset Formula $\bot$
\end_inset

 and 
\begin_inset Formula $\top$
\end_inset

.
\end_layout

\begin_layout Standard
Types do not form a separate syntax class, but are defined as proper terms
 whose type is 
\begin_inset Formula $\mathfrak{T}$
\end_inset

.
\end_layout

\begin_layout Subsection*
Computation terms
\end_layout

\begin_layout Standard
A computation term 
\begin_inset Formula $e$
\end_inset

 is either :
\end_layout

\begin_layout Itemize
an application: 
\begin_inset Formula $e\,t$
\end_inset

 with 
\begin_inset Formula $e$
\end_inset

 a computation term, applied to a propert term 
\begin_inset Formula $t$
\end_inset


\end_layout

\begin_layout Itemize
a variable occurrence : 
\begin_inset Formula $x$
\end_inset


\end_layout

\begin_layout Itemize
an annotation : 
\begin_inset Formula $t:T$
\end_inset

 asserting that term 
\begin_inset Formula $t$
\end_inset

 must have type 
\begin_inset Formula $T$
\end_inset


\end_layout

\begin_layout Itemize
a boolean eliminator : 
\begin_inset Formula $\mathsf{if}\,t\,\mathsf{as}\,x\,\mathsf{return}\,T\,\mathsf{then}\,t_{1}\,\mathsf{else}\,t_{2}$
\end_inset

 with 
\begin_inset Formula $t,t_{1},t_{2}$
\end_inset

 proper terms, 
\begin_inset Formula $x$
\end_inset

 a variable and 
\begin_inset Formula $T$
\end_inset

 a type.
\end_layout

\begin_layout Section
Type checking
\end_layout

\begin_layout Standard
We define a bidirectional type-checking proof system, separated in two parts
 :
\end_layout

\begin_layout Itemize
the type-checking part with rules for proper terms
\end_layout

\begin_layout Itemize
the type-synthesis part with rules for computation terms
\end_layout

\begin_layout Standard
Note that type synthesis here is distinct from what is commonly called type
 inference in non-dependent programming languages.
 We do not introduce an inference scheme here, for the simple reason that
 it is not decidable in the general case.
\end_layout

\begin_layout Subsection
Type-checking
\end_layout

\begin_layout Standard
The goal of a type-checking rule is of the following form:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\underbrace{\left\{ \Gamma\vdash\mathsf{Valid}\land\Gamma\vdash\mathfrak{T}\ni T\land\text{nf}(T)\right\} }_{\text{preconditions}}\quad\underbrace{\Gamma}_{\text{contexte}}\vdash\underbrace{T}_{\text{type}}\ni\underbrace{t}_{\text{term}}\quad\underbrace{\left\{ \right\} }_{\text{postconditions}}
\]

\end_inset


\end_layout

\begin_layout Standard
If the preconditions are satisfied, meaning:
\end_layout

\begin_layout Itemize
the context 
\begin_inset Formula $\Gamma$
\end_inset

 is valid (cf.
 below)
\end_layout

\begin_layout Itemize
\begin_inset Formula $T$
\end_inset

 is a proper type
\end_layout

\begin_layout Standard
Then, we check that the propert term 
\begin_inset Formula $t$
\end_inset

 is indeed of type 
\begin_inset Formula $T$
\end_inset

 in the context of 
\begin_inset Formula $\Gamma$
\end_inset

.
 Note that there is no output here : 
\begin_inset Formula $\Gamma$
\end_inset

, 
\begin_inset Formula $T$
\end_inset

 and 
\begin_inset Formula $t$
\end_inset

 are all inputs of the statement.
\end_layout

\begin_layout Subsubsection*
Context validation
\end_layout

\begin_layout Standard
The context is either the empty context 
\begin_inset Formula $\mathcal{E}$
\end_inset

 or an environment followed by a variable declaration 
\begin_inset Formula $\Gamma,x:T$
\end_inset

.
\end_layout

\begin_layout Standard
The context validation rule are as follows.
\end_layout

\begin_layout Standard
First the empty environment is valid.
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{}{\mathcal{E}\vdash\mathsf{Valid}}
\]

\end_inset


\end_layout

\begin_layout Standard
We then have the following inductive rule:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{\Gamma\vdash\mathsf{Valid}\quad\Gamma\vdash\mathfrak{T}\ni T\quad\text{nf}(T)}{\Gamma,x:T\vdash\mathsf{Valid}}
\]

\end_inset


\end_layout

\begin_layout Standard
Note that this rule depends on the type-checking rules defined below.
\end_layout

\begin_layout Subsubsection*
Type checking rules
\end_layout

\begin_layout Standard
Lambda abstractions
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{\Gamma,x:A\vdash B[x/a]\ni t}{\Gamma\vdash(a:A)\rightarrow B\ni\lambda x.t}
\]

\end_inset


\end_layout

\begin_layout Standard
Type abstractions
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{\Gamma\vdash\mathfrak{T}\ni A\quad\Gamma,a:\tilde{A}\vdash\mathfrak{T}\ni B}{\Gamma\vdash\mathfrak{T}\ni(a:A)\rightarrow B}
\]

\end_inset


\end_layout

\begin_layout Standard
Computation term
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{\Gamma\vdash e\in{\color{blue}T'}\quad T\stackrel{?}{=}{\color{blue}T'}}{\Gamma\vdash T\ni e}
\]

\end_inset


\end_layout

\begin_layout Standard

\series bold
Remark
\series default
: this rule uses a type-synthesis assumption, and a type (term) equivalence.
\end_layout

\begin_layout Standard
Type of type
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{}{\Gamma\vdash\mathfrak{T}\ni\mathfrak{T}}
\]

\end_inset


\end_layout

\begin_layout Standard
Bool type
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{}{\Gamma\vdash\mathfrak{T}\ni\mathbb{B}}
\]

\end_inset


\end_layout

\begin_layout Standard
Boolean constants
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{}{\Gamma\vdash\mathbb{B}\ni\bot}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{}{\Gamma\vdash\mathbb{B}\ni\top}
\]

\end_inset


\end_layout

\begin_layout Subsection
Type synthesis
\end_layout

\begin_layout Standard
The goal of a type-synthesis rule is of the following form:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\underbrace{\left\{ \Gamma\vdash\mathsf{Valid}\right\} }_{\text{preconditions}}\quad\Gamma\vdash\underbrace{t}_{\text{term}}\in\underbrace{{\color{blue}T}}_{\text{{\color{blue}synthesizedtype}}}\quad\underbrace{\left\{ \Gamma\vdash\mathfrak{T}\ni{\color{blue}T}\land\text{nf}({\color{blue}T})\right\} }_{\text{postconditions}}
\]

\end_inset


\end_layout

\begin_layout Standard
The term 
\begin_inset Formula ${\color{blue}T}$
\end_inset

 is here an output of the rule (we use the blue color for outputs).
\end_layout

\begin_layout Standard
Applications
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{\Gamma\vdash e\in{\color{blue}(a:A)\rightarrow B}\quad\Gamma\vdash A\ni t}{\Gamma\vdash e\,t\in{\color{blue}\widetilde{B[t/a]}}}
\]

\end_inset


\end_layout

\begin_layout Standard
Variable occurences
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{\Gamma(x)=T}{\Gamma\vdash x\in{\color{blue}T}}
\]

\end_inset


\end_layout

\begin_layout Standard
Assertions
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{\Gamma\vdash\mathfrak{T}\ni T\quad\Gamma\vdash\tilde{T}\ni t}{\Gamma\vdash t:T\in{\color{blue}\tilde{T}}}
\]

\end_inset


\end_layout

\begin_layout Standard
Boolean eliminator
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\dfrac{\Gamma\vdash\mathbb{B}\ni t\quad\Gamma\vdash\widetilde{T[\top/x]}\ni t_{1}\quad\Gamma\vdash\widetilde{T[\bot/x]}\ni t_{2}}{\Gamma\vdash\mathsf{if}\,t\,\mathsf{as}\,x\,\mathsf{return}\,T\,\mathsf{then}\,t_{1}\,\mathsf{else}\,t_{2}\in{\color{blue}\widetilde{T[t/x]}}}
\]

\end_inset


\end_layout

\begin_layout Subsection
Examples
\end_layout

\begin_layout Paragraph
Example 1
\end_layout

\begin_layout Standard
This exemple does not use dependent types.
\end_layout

\begin_layout Standard
We consider the following environment:
\end_layout

\begin_layout Standard
\begin_inset Formula $\Gamma=\{f:\mathbb{B}\rightarrow\mathbb{B}\}$
\end_inset


\end_layout

\begin_layout Standard
and the following term to be synthesized:
\end_layout

\begin_layout Standard
\begin_inset Formula $\lambda x.f\,x:\mathbb{B}\rightarrow\mathbb{B}$
\end_inset


\end_layout

\begin_layout Standard
We must apply the assertion rule.
\end_layout

\begin_layout Itemize
\begin_inset Formula $\mathbb{B}\ni\mathfrak{T}$
\end_inset

 (bool type) hence 
\begin_inset Formula $\mathfrak{T}\ni\mathbb{B}\rightarrow\mathbb{B}$
\end_inset

(type abstraction)
\end_layout

\begin_layout Itemize
\begin_inset Formula $\Gamma,x:\mathbb{B}$
\end_inset

 and 
\begin_inset Formula $f\in\mathbb{B\rightarrow B}$
\end_inset

 (variable occurrence) and 
\begin_inset Formula $x\in\mathbb{B}$
\end_inset

(variable occurrence) hence 
\begin_inset Formula $f\,x\in\mathbb{B}$
\end_inset

(application) and 
\begin_inset Formula $\mathbb{B}\ni\lambda x.f\,x$
\end_inset

 (lambda abstraction).
\end_layout

\begin_layout Standard
We can conclude 
\begin_inset Formula $\lambda f.f\,x:(a:\mathbb{B})\rightarrow\mathbb{B}\in\mathbb{B}\rightarrow\mathbb{B}$
\end_inset


\end_layout

\begin_layout Section
Inductives
\end_layout

\begin_layout Standard
In this section we introduce the inductives.
\end_layout

\begin_layout Subsection
Descriptors
\end_layout

\begin_layout Standard
A descriptor 
\begin_inset Formula $D$
\end_inset

 of type 
\begin_inset Formula $\mathfrak{D}$
\end_inset

 is a type (sort 
\begin_inset Formula $\mathfrak{T}$
\end_inset

) built according to the following grammar :
\end_layout

\begin_layout Itemize
\begin_inset Formula $`1$
\end_inset

 (
\emph on

\begin_inset Quotes eld
\end_inset

quote one
\begin_inset Quotes erd
\end_inset


\emph default
) is the unit descriptor
\end_layout

\begin_layout Itemize
\begin_inset Formula $`X$
\end_inset

 is a constant descriptor
\end_layout

\begin_layout Itemize
\begin_inset Formula $`\Pi S.T$
\end_inset

 with 
\begin_inset Formula $S$
\end_inset

 a type and 
\begin_inset Formula $T$
\end_inset

 a function of type 
\begin_inset Formula $s\rightarrow\mathfrak{D}$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FormulaMacro
\newcommand{\dtrans}[2]{[\![#1]\!]_{#2}}
\end_inset


\end_layout

\begin_layout Standard
A description is a term of the form 
\begin_inset Formula $\dtrans DX$
\end_inset

 with 
\begin_inset Formula $D$
\end_inset

 a descriptor of type 
\begin_inset Formula $\mathfrak{D}$
\end_inset

 and 
\begin_inset Formula $X$
\end_inset

 is a type.
\end_layout

\begin_layout Standard
The evaluation rules for descriptions are as follows:
\end_layout

\begin_layout Itemize
\begin_inset Formula $\widetilde{\dtrans{`1}X}=\mathcal{U}$
\end_inset

 evaluates to the unit 
\begin_inset Formula $\mathcal{U}$
\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Formula $\widetilde{\dtrans{`X}X}=X$
\end_inset

 evaluates to the argument 
\begin_inset Formula $X$
\end_inset


\end_layout

\begin_layout Itemize
\begin_inset Formula $\widetilde{\dtrans{`\Pi S.T}X}=(s:S)\rightarrow\dtrans{T\,s}X$
\end_inset

 evaluates to a product.
\end_layout

\begin_layout Subsection
Type-checking rules
\end_layout

\begin_layout Standard
A descriptor is a type: 
\begin_inset Formula $\dfrac{}{\mathfrak{T}\ni\mathfrak{D}}$
\end_inset


\end_layout

\begin_layout Standard
Type (checking? synthesis?) rule for descriptions : ???
\end_layout

\begin_layout Standard
The fixpoint constructor: 
\begin_inset Formula $\dfrac{\mathfrak{D}\ni D}{\mathfrak{T}\ni\mu D}$
\end_inset


\end_layout

\begin_layout Standard
The realization (?): 
\begin_inset Formula $\dfrac{\dtrans D{}\mu D\ni x}{\mu D\ni\langle x\rangle}$
\end_inset


\end_layout

\end_body
\end_document
