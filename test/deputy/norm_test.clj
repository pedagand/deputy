
(ns deputy.norm-test

  (:require [clojure.test :as test :refer [deftest is]]
            [deputy.norm :as n :refer :all]))

(deftest eval-simples
  (is (= (evaluation :type) :type))
  (is (= (evaluation :bool) :bool))
  (is (= (evaluation true) true))
  (is (= (evaluation false) false))
  (is (= (evaluation :unit) :unit))
  (is (= (evaluation nil) nil))

  ;; with annotation (same as without)
  (is (= (evaluation '(the :type :type)) :type))
  (is (= (evaluation '(the :bool true)) true))
)

(deftest eval-vars
  ;; free vars
  (is (= (evaluation 'x) [::n/vfree 'x]))
  ;; bound vars
  (is (= (evaluation [:unit] #{0}) :unit))
  (is (= ((evaluation [:unit]'(λ #{0})) 42) 42))
  (is (= ((evaluation [:unit]'(λ #{1})) 42) :unit))
)

(deftest eval-pis
  (is (let [[pi A B] (evaluation '(Π :bool (λ :bool)))]
        (and (= pi ::n/vpi)
             (= A :bool)
             (= (B nil) :bool))))

  (is (let [[_ A B] (evaluation '(Π :bool (λ #{0})))]
        (and (= A :bool)
             (= (B true) true))))

  (is (let [[_ A B] (evaluation '(Π :type (λ (Π #{0} (λ #{0})))))
            [_ C D] (B :unit)]
        (and (= A :type)
             (= C :unit)
             (= (D nil) nil))))

  (is (let [[_ A B] (evaluation '(Π :type (λ (Π #{0} (λ #{1})))))
            [_ C D] (B :unit)]
        (and (= A :type)
             (= C :unit)
             (= (D nil) :unit))))

  (is (let [[_ A B] (evaluation [:bool] '(Π :type (λ (Π #{0} (λ #{2})))))
            [_ C D] (B :unit)]
        (and (= A :type)
             (= C :unit)
             (= (D nil) :bool))))
  )


(deftest eval-lambdas
  (is (let [f (evaluation '(λ #{0}))]
        (= (f 42) 42)))

  (is (let [f (evaluation '(λ (λ #{0})))]
        (= ((f 42) 17) 17)))

  (is (let [f (evaluation '(λ (λ #{1})))]
        (= ((f 42) 17) 42)))

  (is (let [f (evaluation '(λ (Π #{0} (λ #{0}))))
            [_ A B] (f :bool)]
        (and (= A :bool)
             (= (B false) false))))

  (is (= (let [f (evaluation '(the (Π bool (λ bool)) (λ #{0})))]
           (f [::n/vfree 'x])) 
         [::n/vfree 'x]))
)


(deftest eval-applications
  (is (= (evaluation '((λ #{0}) :bool)) :bool))

  (is (= (evaluation '(x y)) 
         '[::n/vapp [::n/vfree x] [::n/vfree y]]))
  
  (is (= (evaluation
                     '((the (Π bool (λ bool)) (λ #{0})) y))
         [::n/vfree 'y]))

  (is (= (((evaluation
             '(λ (λ #{1}))) true) false)
         true))
  
  (is (= (((evaluation
             '(λ (λ #{0}))) true) false)
         false))

)


(deftest quot-simples
  (is (= (quot :type :type) :type))
  (is (= (quot :type :bool) :bool))
  (is (= (quot :bool true) true))
  (is (= (quot :bool false) false))
  (is (= (quot :type :unit) :unit))
  (is (= (quot :unit nil) nil))
)

(deftest quot-pi
  (is (let [pi (evaluation '(Π :bool (λ :bool)))]
        (= (quot :type pi)
           '(Π :bool (λ :bool)))))
)

(deftest quot-lambdas
  (is (let [f (evaluation '(λ #{0}))
            fty (evaluation '(Π :bool (λ :bool)))]
        (= (quot fty f)
           '(λ #{0})))) 

  (is (let [fty (evaluation '(Π :bool (λ :bool)))]
        (= (quot fty (fn [x] x))
           '(λ #{0})))) 

  (is (let [f (evaluation '(λ (λ #{0})))
            fty (evaluation '(Π :bool (λ (Π :bool (λ :bool)))))]
        (= (quot fty f)
           '(λ (λ #{0})))))

  (is (let [fty (evaluation '(Π :bool (λ (Π :bool (λ :bool)))))]
        (= (quot fty (fn [x] (fn [y] y)))
           '(λ (λ #{0})))))
        
  (is (let [f (evaluation '(λ (λ #{1})))
            fty (evaluation '(Π :bool (λ (Π :bool (λ :bool)))))]
        (= (quot fty f)
           '(λ (λ #{1})))))

  (is (let [fty (evaluation '(Π :bool (λ (Π :bool (λ :bool)))))]
        (= (quot fty (fn [x] (fn [y] x)))
           '(λ (λ #{1})))))
)
