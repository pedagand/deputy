
(ns deputy.typing-test

  (:require [clojure.test :as test :refer [deftest is]]
            [deputy.syntax :as s]
            [deputy.norm :as n :refer [evaluation]]
            [deputy.typing :as t :refer :all]))


(deftest typing-core
  ;; Type
  (is (t/type-check {} :type :type) true)

  ;; Unit
  (is (t/type-check {} :type :unit) true)
  (is (t/type-check {} :unit nil) true)

  ;; Pi
  (is (t/type-check {'A :type
                     'B (n/evaluation '(Π A (λ :type)))}
                    :type '(Π A B))
      true)
  (is (t/type-check {'A :type
                     'B (n/evaluation '(Π A (λ :type)))
                     'b (n/evaluation '(Π A (λ (B #{0}))))}
                    (n/evaluation '(Π A (λ (B #{0}))))
                    '(λ (b #{0})))
      true)

  ;; Sigma
  (is (t/type-check {'A :type
                     'B (n/evaluation '(Π A (λ :type)))}
                    :type '(Σ A B))
      true)
  (is (t/type-check {'A :type
                     'B (n/evaluation '(Π A (λ :type)))
                     'a (n/evaluation 'A)
                     'b (n/evaluation '(B a))}
                    (n/evaluation '(Σ A (λ (B #{0}))))
                    '[a b])
      true)
  (is (t/type-synthesis {'A :type
                         'B (n/evaluation '(Π A (λ :type)))
                         'p (n/evaluation '(Σ A (λ (B #{0}))))}
                        '(π1 p))
      'A)
  (is (t/type-synthesis {'A :type
                         'B (n/evaluation '(Π A (λ :type)))
                         'p (n/evaluation '(Σ A (λ (B #{0}))))}
                        '(π2 p))
      '(B (π1 p))))

(deftest type-enumerations
  ;; Label
  (is (t/type-check {} :type :label) true)
  (is (t/type-check {} :label :foo) true)
  (is (t/type-check {} :label :bar) true)

  ;; Labels
  (is (t/type-check {} :type :labels) true)
  (is (t/type-check {} :labels nil) true)
  (is (t/type-check {'t :label
                     'l :labels}
                    :labels '[t l])
      true)
  ;;; special case: close set of labels
  (is (t/type-check {} :labels '(:x :y :z))
      true)

  ;; Enum
  (is (t/type-check {'l :labels}
                    :type '(enum l))
      true)
  (is (t/type-check {'t :label
                     'l :labels}
                    (n/evaluation '(enum [t l]))
                    '0)
      true)
  (is (t/type-check {'t :label
                     'l :labels
                     'n (n/evaluation '(enum l))}
                    (n/evaluation '(enum [t l]))
                    '(suc n))
      true)

  ;; Record
  (is (t/type-synthesis {'l :labels
                         'B (n/evaluation '(Π (enum l) (λ :type)))}
                    '(record l B))
      :type)

  ;; Case
  (is (t/type-synthesis {'l :labels
                         'B (n/evaluation '(Π (enum l) (λ :type)))
                         'e (n/evaluation '(enum l))
                         'cs (n/evaluation '(record l (λ (B #{0}))))}
                    '(case [e B] cs))
      '(B e)))

(deftest type-descriptions
  ;; Desc
  (is (t/type-check {} :type :desc) true)
  (is (t/type-check {} :desc ":unit") true)
  (is (t/type-check {} :desc "X") true)
  (is (t/type-check {'S :type
                     'D (n/evaluation '(Π S (λ :desc)))}
                    :desc '(":Σ" S D)) true)
  (is (t/type-check {'S :type
                     'D (n/evaluation '(Π S (λ :desc)))}
                    :desc '(":Π" S D)) true)

  ;; Interp
  (is (t/type-synthesis {'D :desc
                         'X :type}
                        '(interp D X)) true)

  ;; Fixpoint
  (is (t/type-check {'D :desc }
                    :type '(μ D)) true)
  (is (t/type-check {'D :desc
                     'xs (n/evaluation '(interp D (μ D)))}
                    (n/evaluation '(μ D))
                    '(con xs)) true)

  ;; TODO: All

  ;; TODO: ind
)

(deftest type-bootstrap-descriptions
  ;; Desc
  (is (t/type-check {} :type s/desc2-type) true)
  (is (t/type-check {} n/vdesc2 s/dunit2-term) true)
  (is (t/type-check {} n/vdesc2 s/dvar2-term) true)
  (is (t/type-check {'S :type
                     'D (n/evaluation (list 'Π 'S (list 'λ s/desc2-type)))}
                    n/vdesc2 (s/dsigma2-term 'S 'D)) true)
  (is (t/type-check {'S :type
                     'D (n/evaluation (list 'Π 'S (list 'λ s/desc2-type)))}
                    n/vdesc2 (s/dpi2-term 'S 'D)) true)

  ;; Interp
  (is (t/type-synthesis {'D n/vdesc2
                         'X :type}
                        '(interp2 D X)) true)

  ;; Fixpoint
  (is (t/type-check {'D n/vdesc2 }
                    :type '(μ2 D)) true)
  (is (t/type-check {'D n/vdesc2
                     'xs (n/evaluation '(interp2 D (μ2 D)))}
                    (n/evaluation '(μ2 D))
                    '(con2 xs)) true)

  ;; TODO: All

  ;; TODO: ind
)
