(ns deputy.typing
  (:require [deputy.utils :as u :refer [example examples do-for-example ok>]]
            [deputy.syntax :as stx]
            [deputy.norm :as n]))

(def +examples-enabled+ true)

;;; Type checking
;;; =============

(declare type-check-pi
         type-check-lambda
         type-check-sigma
         type-check-pair
         type-check-type
         type-synthesis)


(defn type-check
  "Checks in context `ctx` if the type `vtype` (in its value form) accepts the canonical term `term`.

  One precondition is that `vtype` is indeed a type in
  `ctx` (i.e. `(type-check ctx :type (quote :type vtype))` shoud be
  ok).

  Returns either `true` or `[:ko msg infos]` in case of a type
  error described by the `msg` message string, and the `infos` map."
  [ctx vtype term]
  ; {:pre [(type-check ctx :type (n/quot :type vtype))]}
  (cond
    ;; Type
    (and (n/vtype? vtype)
         (stx/type-type? term)) true

    ;; Π-type
    (and (n/vtype? vtype)
         (stx/pi-type? term))
    (let [[_ A B] term]
      (type-check-pi ctx A B))
    
    ;; lambda abstractions
    (and (n/vpi? vtype) 
         (stx/lambda-term? term))
    (let [[_ vA vB] vtype
          [_ t] term]
      (type-check-lambda ctx vA vB t))

    ;; Σ-type
    (and (n/vtype? vtype)
         (stx/sigma-type? term))
    (let [[_ A B] term]
      (type-check-sigma ctx A B))

    ;; pair
    (and (n/vsigma? vtype)
         (stx/pair-term? term))
    (let [[_ vA vB] vtype
          [t1 t2] term]
      (type-check-pair ctx vA vB t1 t2))

    ;; bool
    (and (n/vtype? vtype)
         (stx/bool-type? term)) true

    ;; bool constants
    (and (n/vbool? vtype)
         (or (stx/true-term? term)
             (stx/false-term? term))) true

    ;; unit
    (and (n/vtype? vtype)
         (stx/unit-type? term)) true

    ;; unit constant
    (and (n/vunit? vtype)
         (stx/nil-term? term)) true

    ;; label
    (and (n/vtype? vtype)
         (stx/label-type? term)) true

    (and (n/vlabel? vtype)
         (stx/tag-term? term)) true

    ;; labels
    (and (n/vtype? vtype)
         (stx/labels-type? term)) true

    ;; labels constructors
    (and (n/vlabels? vtype)
         (stx/labels-term? term)) true

    (and (n/vlabels? vtype)
         (stx/nil-term? term)) true

    (and (n/vlabels? vtype)
         (stx/pair-term? term))
    (let [[t l] term]
      (ok> (type-check ctx :label t)
           [:ko> "Bad label." {:term t}]
           (type-check ctx vtype l)
           [:ko> "Bad labels." {:term l}]
           true))

    ;; enums
    (and (n/vtype? vtype)
         (stx/enum-type? term))
    (let [[_ l] term]
      (ok> (type-check ctx :labels l)
           [:ko> "Bad labels." {:term l}]
           true))

    (and (n/venum? vtype)
         (stx/zero-term? term))
    (let [[_ l] vtype]
      (if (not (n/vpair? l))
           [:ko "Expects a non-empty set of labels." {:term l}]
           true))

    (and (n/venum? vtype)
         (stx/suc-term? term))
    (let [[_ l] vtype
          [_ n] term]
      (if (not (n/vpair? l))
           [:ko "Expects a non-empty set of labels." {:term l}]
           (let [[_ l2] l]
             (type-check ctx [::n/venum l2] n))))

    ;; Desc
    (and (n/vtype? vtype)
         (stx/desc-type? term)) true

    (and (n/vdesc? vtype)
         (or (stx/dunit-term? term)
             (stx/dvar-term? term))) true

    (and (n/vdesc? vtype)
         (or (stx/dsigma-term? term)
             (stx/dpi-term? term)))
    (let [[dquant S D] term]
      (ok> (type-check ctx :type S)
           [:ko> "Invalid domain." {:quant dquant :term S}]
           (type-check ctx [::n/vpi (n/evaluation S) (fn [_] :desc)] D)
           [:ko> "Invalid codomain." {:quant dquant :term D}]
           true))

    ;; Fixpoint
    (and (n/vtype? vtype)
         (stx/mu-type? term))
    (let [[_ D] term]
      (ok> (type-check ctx :desc D)
           [:ko> "Invalid description." {:desc D}]
           true))

    (and (n/vmu? vtype)
         (stx/con-term? term))
    (let [[_ xs] term
          [_ D] vtype]
      (ok> (type-check ctx (n/vinterp D vtype) xs)
           [:ko> "Invalid arguments" {:args xs :vtype (n/vinterp D vtype)}]
           true))

    ;; Bootstrapped

    (and (n/vtype? vtype)
         (stx/mu2-type? term))
    (let [[_ D] term]
      (ok> (type-check ctx n/vdesc2 D)
           [:ko> "Invalid description." {:desc D}]
           true))

    (and (n/vmu2? vtype)
         (stx/con2-term? term))
    (let [[_ xs] term
          [_ D] vtype]
      (ok> (type-check ctx (n/vinterp2 D vtype) xs)
           [:ko> "Invalid arguments" {:args xs :vtype (n/vinterp2 D vtype)}]
           true))

    ;; errors
    (stx/canonical-term? term)
    [:ko "Wrong type for canonical term." {:term term
                                           :type (n/quot ctx :type vtype)}]

    ;; end with neutral terms
    :else
    (ok> (type-synthesis ctx term) :as vtype'
         [:ko> "Cannot synthesize type of computation term." {:term term}]
         (if (not= (n/quot ctx :type vtype') (n/quot ctx :type vtype))
           [:ko "Wrong type for computation term." {:term term
                                                    :synthesized-type (n/quot ctx :type vtype')
                                                    :type (n/quot ctx :type vtype)}]
           true))))

(defn type-check-lambda
  "Checks that `(λ t)` is of type `(Π A B)`."
  [ctx vA vB t]
  (let [x (gensym "lam")
        v [::n/vfree x]
        ctx' (assoc ctx x vA)]
  (ok> (type-check ctx' (vB v) (stx/subst x t))
       [:ko> "Type error for abstraction." {:term (list 'λ t)
                                            :type (n/quot ctx :type [::n/vpi vA vB])}])))

(examples
 (type-check {} (n/evaluation '(Π :type (λ (Π #{0} (λ :type))))) '(λ (λ :bool)))
 => true)

(defn type-check-pi
  "Checks that `(Π A B)` is a type."
  [ctx A B]
  (ok> (type-check ctx :type A)
       [:ko> "Domain of Pi-type is not a type." {:domain A}]
       (type-check ctx [::n/vpi (n/evaluation A) (fn [_] :type)] B)
       [:ko> "Codomain of Pi-type is not a type." {:codomain B}]))

(examples
 (type-check {} :type '(Π :bool (λ :bool)))
 => true

;; (type-check {} :type '(Π :bool (if [#{0} :type] :unit :bool))) => :ok
)


(defn type-check-sigma
  "Checks that `(Σ A B)` is a type."
  [ctx A B]
  (let [x (gensym "sigma")]
    (ok> (type-check ctx :type A)
         [:ko> "Domain of Sigma-type is not a type." {:domain A}]
         (type-check ctx [::n/vpi (n/evaluation A) (fn [_] :type)] B)
         [:ko> "Codomain of Sigma-type is not a type." {:codomain B}])))

(examples
 (type-check {} :type '(Σ :bool (λ :bool)))
 => true)

(defn type-check-pair
  "Checks that `[t1 t2]` is of type `(Σ A B)`."
  [ctx vA vB t1 t2]
  (ok> (type-check ctx vA t1)
       [:ko> "Type error for first component." {:term t1
                                                :type (n/quot ctx :type vA)}]
       (vB (n/evaluation t1)) :as vT2
       (type-check ctx vT2 t2)
       [:ko> "Type error for second component." {:term t2
                                                :type (n/quot ctx :type vT2)}]))

(examples
 (type-check {} (n/evaluation '(Σ :bool (λ (if [#{0} (λ :type)] :unit :bool))))
             [true nil])
 => true

 (type-check {} (n/evaluation '(Σ :bool (λ (if [#{0} (λ :type)] :unit :bool))))
             [true false])
 => [:ko "Type error for second component."
     {:term false, :type :unit,
      :cause [:ko "Wrong type for canonical term." {:term false, :type :unit}]}]

 (type-check {} (n/evaluation '(Σ :bool (λ (if [#{0} (λ :type)] :unit :bool))))
             [false nil])
 => [:ko "Type error for second component."
     {:term nil, :type :bool,
      :cause [:ko "Wrong type for canonical term." {:term nil, :type :bool}]}])

;;; Type synthesis
;;; ==============

(declare type-synth-app
         type-synth-annot
         type-synth-if
         type-synth-proj1
         type-synth-proj2
         type-synth-record
         type-synth-case
         type-synth-caseD)

(defn type-synthesis
  "Synthesizes the type of computation term `term` in context `ctx`.

  Returns either `[:ok vtype nil]` with `vtype` the synthesized type or
  an error `[:ko msg info]`."
  [ctx term]
  (cond
    (stx/freevar? term)
    (get ctx term [:ko "No such variable in context" {:var term}])

    (stx/application? term)
    (let [[e t] term]
      (type-synth-app ctx e t))

    (stx/annotation? term)
    (let [[_ T t] term]
      (type-synth-annot ctx T t))

    (stx/if? term)
    (let [[_ [c T] t1 t2] term]
      (type-synth-if ctx c T t1 t2))

    (stx/proj1? term)
    (let [[_ t] term]
      (type-synth-proj1 ctx t))

    (stx/proj2? term)
    (let [[_ t] term]
      (type-synth-proj2 ctx t))

    (stx/record-type? term)
    (let [[_ t T] term]
      (type-synth-record ctx t T))

    (stx/case-term? term)
    (let [[_ [t T] cs] term]
      (type-synth-case ctx t T cs))

    (stx/interp-term? term)
    (let [[_ D X] term]
      (ok> (type-check ctx :desc D)
           [:ko "Invalid description" {:desc D}]
           (type-check ctx :type X)
           [:ko "Invalid recursive argument" {:rec X}]
           :type))

    ;; Bootstrapped
    (stx/caseD-term? term)
    (let [[_ t cs] term]
      (type-synth-caseD ctx t cs))

    (stx/interp2-term? term)
    (let [[_ D X] term]
      (ok> (type-check ctx n/vdesc2 D)
           [:ko "Invalid description" {:desc D}]
           (type-check ctx :type X)
           [:ko "Invalid recursive argument" {:rec X}]
           :type))

    :else (throw (ex-info "Type synthesis failed (unsupported term)." {:term term}))))

(examples
 (type-synthesis '{x :bool} 'x) => :bool
 
 (type-synthesis '{x :bool} 'y)
 => [:ko "No such variable in context" '{:var y}])

(defn type-synth-app
  "Synthesizes type of application with operator `e` and operand `t`."
  [ctx e t]
  (ok> (type-synthesis ctx e) :as etype
       [:ko> "Cannot synthesize type of left hand-side (operator) of a function." {:operator e}]
       (when-not (n/vpi? etype)
         [:ko "Left hand-side (operator) of application must be a Pi-type." {:operator e
                                                                             :type etype}])
       etype :as [_ A B]
       (assert (fn? B))
       (type-check ctx A t)
       [:ko> "Wrong domain for operand." [:domain A, :term t]]
       (n/vapp B (n/evaluation t))))

(examples
 (type-synthesis {'f [::n/vpi :bool (fn [x] :bool)]
                  'x :bool}
                 '(f x)) => :bool

 (type-check {'f [::n/vpi :bool (fn [x] :bool)]
              'x :bool}
             :bool '(f x)) => true)

(defn type-synth-annot
  "Synthesizes type of annotation `(the T t)` asserting that
  term `t` has type `T`."
  [ctx T t]
  (ok> (type-check ctx :type T)
       [:ko> "Wrong annotation: not a type" {:annot-type T, :annot-term t}]
       (n/evaluation T) :as vT
       (type-check ctx vT t)
       [:ko> "Annotation type error" {:annot-type T, :annot-term t}]
       vT))

(examples
 (type-synthesis {} '(the :bool true)) => :bool
)

(defn type-synth-if
  "Synthesizes type of boolean elimination `(if [c T] tthen telse)`."
  [ctx c T tthen telse]
  (ok> (type-check ctx :bool c)
       [:ko> "Condition of `if` is not a boolean" {:condition c}]
       (type-check ctx [::n/vpi :bool (fn [_] :type)] T)
       [:ko> "Return of `if` is not a type." {:motive T}]
       (n/evaluation T) :as vT
       (vT true) :as vTtrue
       (type-check ctx vTtrue tthen)
       [:ko> "Wrong type for then branch of `if`." {:then-term tthen
                                                    :then-type (n/quot ctx :type vTtrue)}]
       (vT false) :as vTfalse
       (type-check ctx vTfalse telse)
       [:ko> "Wrong type for else branch of `if`." {:else-term telse
                                                    :else-type (n/quot ctx :type vTfalse)}]
       (vT (n/evaluation c))))

(examples
 (type-synthesis {} '(if [true (λ :bool)] false true))
 => :bool
 (type-synthesis {} '(if [true (λ (if [#{0} (λ :type)] :bool :unit))] true nil))
 => :bool
 (type-synthesis {} '(the (if [true (λ :type)] :bool :unit) true)) => :bool)

(defn type-synth-proj1
  "Synthesizes type of first projection over `t`."
  [ctx t]
  (ok> (type-synthesis ctx t) :as vT
       (assert (n/vsigma? vT))
       (let [[_ vA vB] vT]
         vA)))

(defn type-synth-proj2
  "Synthesizes type of second projection over `t`."
  [ctx t]
  (ok> (type-synthesis ctx t) :as vT
       (assert (n/vsigma? vT))
       (let [[_ vA vB] vT]
         (vB (n/vproj1 (n/evaluation t))))))

(examples
 (type-synthesis {'x (n/evaluation '(Σ :bool (λ :unit)))} '(π1 x))
 => :bool
 (type-synthesis {'x (n/evaluation '(Σ :unit (λ :bool)))} '(π2 x))
 => :bool
 (type-synthesis {'x (n/evaluation '(Σ (Σ :bool (λ :unit)) (λ :unit)))} '(π1 (π1 x)))
 => :bool
 (type-synthesis {'x (n/evaluation '(Σ :unit (λ (Σ :unit (λ :bool)))))} '(π2 (π2 x)))
 => :bool
 (type-synthesis {'x (n/evaluation '(Σ (Σ :unit (λ :bool)) (λ :unit)))} '(π2 (π1 x)))
 => :bool)

(defn type-synth-record
  "Synthesizes type of record over labels `t`."
  [ctx t T]
  (ok> (type-check ctx :labels t)
       [:ko> "Bad labels" {:labels t}]
       (type-check ctx
                   [::n/vpi [::n/venum (n/evaluation t)] (fn [_] :type)]
                   T)
       [:ko> "Bad motive" {:motive T}]
       :type))

(examples
 (type-synthesis {} '(record nil (λ :type)))
 => :type
 (type-synthesis {} '(record [:x [:y [:z nil]]] (λ :type)))
 => :type)

(defn type-synth-case
  "Synthesizes type of case over index `t`."
  [ctx ne T cs]
  (ok> (type-synthesis ctx ne) :as netype
       [:ko> "Cannot synthesize type of left hand-side (operator) of a case."
        {:operator ne}]
       (when-not (n/venum? netype)
         [:ko "Left hand-side (operator) of application must be an Enum-type."
          {:operator ne
           :type netype}])
       netype :as [_ vl]
       
       (type-check ctx [::n/vpi [::n/venum vl] (fn [_] :type)] T)
       [:ko> "Bad motive" {:motive T}]
       ;; XXX: HACK must go away with elaboration
       (n/evaluation (list 'λ (list T #{0}))) :as vT

       (type-check ctx (n/vrecord vl vT) cs)
       [:ko> "Bad cases" {:cases cs}]
       
       (vT (n/evaluation ne))))


(examples
 (type-synth-case {'x '[::n/venum [:x [:y [:z nil]]]]}
                  'x '(λ :type) [:unit [:bool [:unit nil]]])
 => :type
 (first (type-synth-case {'x '[::n/venum [:x [:y [:z nil]]]]}
                  'x '(λ :type) [:unit [:bool :unit]]))
 => :ko)

(defn type-synth-caseD
  "Synthesizes type of case over index `t` with target Desc."
  [ctx ne cs]
  (ok> (type-synthesis ctx ne) :as netype
       [:ko> "Cannot synthesize type of left hand-side (neutral) of a case."
        {:operator ne}]
       (when-not (n/venum? netype)
         [:ko "Left hand-side (operator) of application must be an Enum-type."
          {:operator ne
           :type netype}])
       netype :as [_ vl]

       (type-check ctx (n/vrecord vl (fn [_] n/vdesc2)) cs)
       [:ko> "Bad cases" {:cases cs}]

       n/vdesc2))


;;; Type-checking examples

(examples
 (type-check {} (n/evaluation '(Π :bool (λ :bool))) '(λ #{0})) => true

;; XXX: problem with comparing outputs of gensym-ed terms!
;;  ;; (type-check {} (n/evaluation '(Π :bool true)) '(λ #{0}))
;;  ;; => [:ko "Type error for abstraction."
;;  ;;     {:term (λ #{0}),
;;  ;;      :type (Π :bool true),
;;  ;;      :cause [:ko "Wrong type for computation term."
;;  ;;              {:term lam17054,
;;  ;;               :synthesized-type :bool,
;;  ;;               :type true}]}]

 (first (type-check {} (n/evaluation '(Π :bool (λ :type))) '(λ #{0})))
 => :ko

 (type-check {} (n/evaluation '(Π :bool (λ (if [#{0} (λ :type)] :unit :unit))))
                '(λ (if [#{0} (λ (if [#{0} (λ :type)] :unit :unit))] nil nil)))
 => true

 (first (type-check {} (n/evaluation '(Π :bool (λ (if [#{0} (λ :type)] :unit :unit))))
                '(λ nil)))
 => :ko

 (first (type-check {} (n/evaluation '(Π :bool (λ (if [#{0} (λ :type)] :unit :unit))))
                    '(λ (if [#{0} :unit] :unit :unit))))
 => :ko

 (type-synthesis {'x :bool
                  'B (n/evaluation '(Π :bool (λ :type)))
                  'ifT (n/evaluation '(B true))
                  'ifF (n/evaluation '(B false))}
                 '(if [x (λ (B #{0}))] ifT ifF))
 => '[:deputy.norm/vapp [:deputy.norm/vfree B] [:deputy.norm/vfree x]]
 (type-synthesis {'x (n/evaluation '(enum [:true [:false nil]]))
                  'B (n/evaluation '(Π (enum [:true [:false nil]]) (λ :type)))
                  'ifT (n/evaluation '(B 0))
                  'ifF (n/evaluation '(B (suc 0)))}
                 '(case [x (λ (B #{0}))] [ifT [ifF nil]]))
 => '[:deputy.norm/vapp [:deputy.norm/vfree B] [:deputy.norm/vfree x]]
)

(examples
 (type-check {} :type :bool) => true
 (type-check {} :type :unit) => true
 (type-check {} :bool true) => true
 (type-check {} :bool false) => true

 (type-check {} :unit true) 
 => '[:ko "Wrong type for canonical term." {:term true, :type :unit}]

 (type-check {} :type :label) => true
 (type-check {} :type :labels) => true
 
 (type-check {} :label ':x) => true
 (type-check {} :labels 'nil) => true
 (type-check {} :labels '[:x nil]) => true
 (type-check {} :labels '[:x [:y nil]]) => true
 (type-check {} :labels '(:x :y)) => true
 (type-check {} :labels '(:x :y :z)) => true

 (type-check {} :type '(enum (:x :y :z))) => true
 (type-check {} [::n/venum [:x [:y [:z nil]]]] 0) => true
 (first (type-check {} [::n/venum nil] 0)) => :ko
 (first (type-check {} [::n/venum [:x]] 0)) => :ko
 (type-check {} [::n/venum [:x [:y [:z nil]]]] '(suc 0)) => true
 (type-check {} [::n/venum [:x [:y [:z nil]]]] '(suc (suc 0))) => true
 (first (type-check {} [::n/venum [:x [:y [:z nil]]]] '(suc (suc (suc 0))))) => :ko
 (type-check {'l :stx/labels} [::n/venum [:x 'l]] '0) => true
 (first (type-check {'l :stx/labels} [::n/venum [:x 'l]] '(suc 0))) => :ko
 (type-check {'l :stx/labels} [::n/venum [:x [:y 'l]]] '(suc 0)) => true
 (first (type-check {'l :stx/labels} [::n/venum [:x [:y 'l]]] '(suc (suc 0)))) => :ko

 (type-check {} :type :desc) => true
 (type-check {} :desc ":unit") => true
 (type-check {} :desc "X") => true
 (type-check {} :desc '(":Σ" :type (λ ":unit"))) => true
 (type-check {} :desc '(":Σ" :bool (λ "X"))) => true
 (type-check {} :desc '(":Σ" :type (λ (":Σ" #{0} (λ ":unit"))))) => true
 (first (type-check {} :desc '(":Σ" :type ":unit"))) => :ko
 (type-check {} :desc '(":Σ"
                        (enum (:Z :S))
                        (λ (case [#{0} (λ :desc)]
                                 [":unit" ["X" nil]])))) => true
 (type-check {}
             (n/evaluation '(record (:Z :S) (λ :desc)))
             [":unit" ["X" nil]]) => true
 (type-check {} :type '(μ
                        (":Σ"
                         (enum (:Z :S))
                         (λ (case [#{0} (λ :desc)]
                              [":unit" ["X" nil]]))))) => true
 (type-check {} (n/evaluation '(μ
                                (":Σ"
                                 (enum (:Z :S))
                                 (λ (case [#{0} (λ :desc)]
                                      [":unit" ["X" nil]])))))
             '(con [0 nil])) => true
 (type-check {} (n/evaluation '(μ
                                (":Σ"
                                 (enum (:Z :S))
                                 (λ (case [#{0} (λ :desc)]
                                      [":unit" ["X" nil]])))))
             '(con [(suc 0) (con [0 nil])])) => true
 (type-check {'n (n/evaluation '(μ
                                (":Σ"
                                 (enum (:Z :S))
                                 (λ (case [#{0} (λ :desc)]
                                      [":unit" ["X" nil]])))))}
             (n/evaluation '(μ
                             (":Σ"
                              (enum (:Z :S))
                              (λ (case [#{0} (λ :desc)]
                                   [":unit" ["X" nil]])))))
             '(con [(suc 0) n])) => true
 (first (type-check {} (n/evaluation '(μ
                                (":Σ"
                                 (enum (:Z :S))
                                 (λ (case [#{0} (λ :desc)]
                                      [":unit" ["X" nil]])))))
             '(con [(suc (suc 0)) (con [0 nil])]))) => :ko

 ;; bootstrap

 (type-check {} :type stx/desc2-type) => true
 (type-check {} n/vdesc2 stx/dunit2-term) => true
 (type-check {} n/vdesc2 stx/dvar2-term) => true
 (type-check {} n/vdesc2 (stx/dsigma2-term :type (list 'λ stx/dunit2-term))) => true
 (type-check {} n/vdesc2 (stx/dsigma2-term :bool (list 'λ stx/dvar2-term))) => true
 (type-check {} n/vdesc2 (stx/dsigma2-term :type (list 'λ (stx/dsigma2-term #{0} (list 'λ stx/dunit2-term))))) => true
 (first (type-check {} n/vdesc2 '(stx/dsigma2-term :type stx/dunit2-term))) => :ko
 (type-check {} n/vdesc2 (stx/dsigma2-term
                          '(enum (:Z :S))
                          (list 'λ (list 'case [#{0} (list 'λ stx/desc2-type)]
                                         [stx/dunit2-term
                                          [stx/dvar2-term
                                           nil]])))) => true
 (type-check {}
             (n/evaluation (list 'record [:Z [:S nil]] (list 'λ stx/desc2-type)))
             [stx/dunit2-term [stx/dvar2-term nil]]) => true
 (type-check {} :type (list 'μ2
                        (stx/dsigma2-term
                         '(enum (:Z :S))
                         (list 'λ
                               (list 'case [#{0} (list 'λ stx/desc2-type)]
                                     [stx/dunit2-term
                                      [stx/dvar2-term
                                       nil]]))))) => true
 (type-check {} (n/evaluation (list 'μ2
                                (stx/dsigma2-term
                                 '(enum (:Z :S))
                                 (list 'λ
                                       (list 'case [#{0} (list 'λ stx/desc2-type)]
                                             [stx/dunit2-term
                                              [stx/dvar2-term
                                               nil]])))))
             '(con2 [0 nil])) => true
 (type-check {} (n/evaluation (list 'μ2
                                    (stx/dsigma2-term
                                     '(enum (:Z :S))
                                     (list 'λ
                                           (list 'case [#{0} (list 'λ stx/desc2-type)]
                                                 [stx/dunit2-term
                                                  [stx/dvar2-term
                                                   nil]])))))
             '(con2 [(suc 0) (con2 [0 nil])])) => true
 (type-check {'n (n/evaluation (list 'μ2
                                (stx/dsigma2-term
                                 '(enum (:Z :S))
                                 (list 'λ
                                       (list 'case [#{0} (list 'λ stx/desc2-type)]
                                             [stx/dunit2-term
                                              [stx/dvar2-term
                                               nil]])))))}
             (n/evaluation (list 'μ2
                             (stx/dsigma2-term
                              '(enum (:Z :S))
                              (list 'λ
                                    (list 'case [#{0} (list 'λ stx/desc2-type)]
                                          [stx/dunit2-term
                                           [stx/dvar2-term
                                            nil]])))))
             '(con2 [(suc 0) n])) => true
 (first (type-check {} (n/evaluation (list 'μ2
                                (stx/dsigma2-term
                                 '(enum (:Z :S))
                                 (list 'λ
                                       (list 'case [#{0} (list 'λ stx/desc2-type)]
                                             [stx/dunit2-term
                                              [stx/dvar2-term
                                               nil]])))))
             '(con2 [(suc (suc 0)) (con2 [0 nil])]))) => :ko
)


;; 1. Describe :desc in :desc

(def DescD0
  '(":Σ"
    (enum [:unit [:X [:Σ [:Π nil]]]])
    (λ (case [#{0} (λ :desc)]
         [":unit"
          [":unit"
            [(":Σ" :type (λ (":Π" #{0} (λ "X"))))
             [(":Σ" :type (λ (":Π" #{0} (λ "X"))))
              nil]]]]))))

(def Desc0 (n/evaluation (list 'μ DescD0)))
(def dunit '(con [0 nil]))
(def dX '(con [(suc 0) nil]))
(defn dSigma [S D] (list 'con [(list 'suc (list 'suc '0))
                               [S D]]))
(defn dPi [S D] (list 'con [(list 'suc (list 'suc (list 'suc '0)))
                            [S D]]))



(examples
 (type-check {} :desc DescD0)
 => true
 (type-check {} :type (list 'μ DescD0))
 => true
 (type-check {} Desc0 dunit)
 => true
 (type-check {} Desc0 dX)
 => true
 (type-check {'S :type
              'D (n/evaluation (list 'Π 'S (list 'λ (list 'μ DescD0))))}
             Desc0 (dSigma 'S 'D))
 => true
 (type-check {'S :type
              'D (n/evaluation (list 'Π 'S (list 'λ (list 'μ DescD0))))}
             Desc0 (dPi 'S 'D))
 => true)

;; 2. Describe :desc in Desc

(def DescD1
  (dSigma
    '(enum [:unit [:X [:Σ [:Π nil]]]])
    (list 'λ (list 'case [#{0} (list 'λ (list 'μ DescD0))]
         [dunit
          [dunit
            [(dSigma :type (list 'λ (dPi #{0} (list 'λ dX))))
             [(dSigma :type (list 'λ (dPi #{0} (list 'λ dX))))
              nil]]]]))))

(examples
 DescD1
 => '(con [(suc (suc 0))
          [(enum [:unit [:X [:Σ [:Π nil]]]]) 
           (λ (case [#{0}
                     (λ (μ (":Σ" (enum [:unit [:X [:Σ [:Π nil]]]]) (λ (case [#{0} (λ :desc)] [":unit" [":unit" [(":Σ" :type (λ (":Π" #{0} (λ "X")))) [(":Σ" :type (λ (":Π" #{0} (λ "X")))) nil]]]])))))]
                [(con [0 nil])
                 [(con [0 nil])
                  [(con [(suc (suc 0))
                         [:type (λ (con [(suc (suc (suc 0)))
                                         [#{0} (λ (con [(suc 0) nil]))]]))]])
                   [(con [(suc (suc 0))
                          [:type (λ (con [(suc (suc (suc 0)))
                                          [#{0} (λ (con [(suc 0) nil]))]]))]])
                    nil]]]]))]])

 (type-check {} Desc0 DescD1)
 => true)

;; 3. Introduce mu2, con2 and interp2

;; 4. Self-referential Desc

(examples
 stx/descD
 => '(con2 [(suc (suc 0))
           [(enum [:unit [:X [:Σ [:Π nil]]]])
            (λ (caseD #{0}
                      [(con2 [0 nil])
                       [(con2 [0 nil])
                        [(con2 [(suc (suc 0))
                                [:type (λ (con2 [(suc (suc (suc 0)))
                                                 [#{0} (λ (con2 [(suc 0) nil]))]]))]])
                         [(con2 [(suc (suc 0))
                                 [:type (λ (con2 [(suc (suc (suc 0)))
                                                  [#{0} (λ (con2 [(suc 0) nil]))]]))]])
                          nil]]]]))]])
 (type-check {} n/vdesc2 stx/descD) => true
 stx/desc2-type => (list 'μ2 stx/descD)
 (type-check {} :type stx/desc2-type) => true
 (type-check {} n/vdesc2 stx/dunit2-term) => true
 (type-check {} n/vdesc2 stx/dvar2-term) => true
 (type-check {'S :type
              'D (n/evaluation (list 'Π 'S (list 'λ stx/desc2-type)))}
             n/vdesc2 (stx/dsigma2-term 'S 'D)) => true
 (type-check {'S :type
              'D (n/evaluation (list 'Π 'S (list 'λ stx/desc2-type)))}
             n/vdesc2 (stx/dpi2-term 'S 'D)) => true)

(comment
  (if [b :as x :return B x] ifT ifF)

  (ddef bool :type (enum :true :false))
  (ddefn ifte [[b :bool]
               [B (-> bool :type)]
               [ifT (B true)]
               [ifF (B false)]]
         (case [b :as x :return B x] ifT ifF)))
