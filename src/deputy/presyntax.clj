(ns deputy.presyntax
  (:require [deputy.utils :as u :refer [example examples ok>]]))

(def +examples-enabled+ true)

;;; Specification of external syntax
;;; ================================

(def reserved-symbols '#{λ lambda fn fun Π pi  -> →
                         the
                         if
                         Σ sig π π1 π2 proj
                         record case caseD
                         interp interp2 con con2 mu mu2 All ind
                         ":Σ" ":Π"})

(defn reserved? [t]
  (or (= (first (name t)) \_) ;; reserved for bound variables
   (contains? reserved-symbols t)))

;; Identifiers
;; -----------

(defn identifier? [t]
  (and (symbol? t)
       (not (reserved? t))))

;; Constants
;; ---------

(defn type-type? [t]
  (= t :type))

(defn unit-type? [t]
  (= t :unit))

(defn bool-type? [t]
  (= t :bool))

(defn true-term? [t]
  (= t true))

(defn false-term? [t]
  (= t false))

;; Lambda abstractions
;; -------------------

(defn lambda? [t]
  (and (u/seq1? t)
       (contains? '#{λ lambda fn fun} (first t))))

(defn parse-lambda [t]
  (ok> (when (not= (count t) 3)
         [:ko> "Wrong arity for lambda-abstraction, expecting 3."
          {:term t
           :arity (count t)}])
       (second t) :as params
       (when (not (and (vector? params)
                       (pos? (count params))
                       (every? identifier? params)))
         [:ko> "Lambda parameters should be a non-empty vector of variable identifiers."
          {:params params}])
       (when (not (= (count (into #{} params)) (count params)))
         [:ko> "Lambda parameters should be all distinct." {:params params}])
       [:ok params (nth t 2)]))
                      
(examples 
 (parse-lambda '(lambda [x y z] t)) => '[:ok [x y z] t]

 (parse-lambda '(lambda t)) 
 => [:ko> "Wrong arity for lambda-abstraction, expecting 3." '{:term (lambda t), :arity 2}]

 (parse-lambda '(lambda [] t))
 => [:ko> "Lambda parameters should be a non-empty vector of variable identifiers." {:params []}]

 (parse-lambda '(lambda [x y x z] t))
 => '[:ko> "Lambda parameters should be all distinct." {:params [x y x z]}]

 (parse-lambda '(lambda (x y z) t))
 => '[:ko> "Lambda parameters should be a non-empty vector of variable identifiers." {:params (x y z)}]
)


;; Pi type
;; --------------------

(defn pi? [t]
  (and (u/seq1? t)
       (contains? '#{Π pi ->  →} (first t))))

(defn dep-pi? [t]
  (and (>= (count t) 2)
       (vector? (second t))))

(defn parse-pi [t]
  (if (dep-pi? t)
    (ok> (when (not= (count t) 3)
         [:ko> "Wrong arity for Pi-type, expecting 3." {:term t
                                                                    :arity (count t)}])
       (second t) :as binding
       (when (not (and (vector? binding)
                       (>= (count binding) 2)))
         [:ko> "Malformed Pi-type binding: missing binding elements." {:binding binding}])
       (butlast binding) :as vars
       (last binding) :as typ
       (when (not (every? identifier? vars))
         [:ko> "Pi-type variables should be identifiers." {:vars vars}])
       (when (not (= (count (into #{} vars)) (count vars)))
         [:ko> "Pi-type variables should be all distinct." {:vars vars}])
       [:ok {:pi-kind ::dependent
             :vars vars 
             :dom typ 
             :cod (nth t 2)}])
    ;; non-dependent function
    (ok> (when (not (>= (count t) 3))
           [:ko> "Malformed arrow type: not enough operands." {:term t}])
         [:ok {:pi-kind ::arrow
               :doms (rest (butlast t))
               :cod (last t)}])))


(examples
 (parse-pi '(-> [a A] B))
 => '[:ok {:pi-kind :deputy.presyntax/dependent
           :vars (a)
           :dom A, :cod B}]

 (parse-pi '(-> [a b c T] U)) 
 => '[:ok {:pi-kind :deputy.presyntax/dependent
           :vars (a b c)
           :dom T, :cod U}]

 (parse-pi '(-> T1 T2 T3 T4))
=> '[:ok {:pi-kind :deputy.presyntax/arrow
          :doms (T1 T2 T3), :cod T4}])

;; Applications
;; ------------

(defn application? [t]
  (and (seq? t)
       (not (reserved? (first t)))
       (not (keyword? (first t)))))

;; Variable
;; --------

(defn variable? [t]
  (and (symbol? t)
       (not (contains? reserved-symbols t))))

;; Annotation
;; ----------

(defn annotation? [t]
  (and (u/seq1? t)
       (= (first t) 'the)))

;; Boolean elimination
;; -------------------

;; (if [c :as x :return T] t1 t2)

(defn if? [t]
  (and (u/seq1? t)
       (= (first t) 'if)))

(defn parse-if [t]
  (ok> (when (not= (count t) 4)
         [:ko> "Wrong arity for if expression, expecting 4." {:term t
                                                              :arity (count t)}])
       t :as [_ u tthen telse]
       (when (or (not (vector? u))
                 (not= (count u) 5))
         [:ko> "Wrong condition vector for if expression." {:condition u}])
       u :as [cnd _ x _ Tret]
       (when (not (identifier? x))
         [:ko> "Condition binder must be an identifier." {:binder x}])
       [:ok cnd x Tret tthen telse]))

(example
 (parse-if '(if [true :as x :return :bool] true false))
 => '[:ok true x :bool true false])

;; pairs
;; ------------

(defn pair? [t]
  (vector? t))

(defn pair-type? [t]
  (and (u/seq1? t)
       (contains? '#{sig Σ} (first t))))

(defn proj? [t]
  (and (u/seq1? t)
       (contains? '#{proj π} (first t))))

(defn parse-binding [t]
  (if (not (vector? t))
    ['_ t]
    (ok> (when (not= (count t) 2)
           [:ko> "Wrong binding: expecting `[var type]` form." {:term t}])
         (when-not (identifier? (first t))
           [:ko> "Binding variable must be an idenfier." {:binding t}])
      t)))


;;; Parser from external to internal syntax
;;; =======================================

(declare unfold-lambdas
         unfold-pis
         unfold-pair
         unfold-pair-type
         unfold-proj
         search-boundvar)

(defn parse
  "Parses `term` from external syntax to internal syntax,
  returns either the result term or an error of the form
  `[:ko <msg> <infos>]`."
  ([term] (parse '() term))
  ([benv term]
   (cond
     ;; constants are also in the internal syntax
     (or (type-type? term)
         (unit-type? term)
         (bool-type? term)
         (true-term? term)
         (false-term? term)) 
     term

     (lambda? term)
     (ok> (parse-lambda term) :as [_ params body]
          (if (> (count params) 1)
            (unfold-lambdas (rest params) body)
            body) :as body'
          (parse (cons (first params) benv) body') :as body''
          (list 'λ body''))

     (pi? term)
     (ok> (parse-pi term) :as [_ p]
          (case (:pi-kind p)
            ::dependent
            (let [cod (if (> (count (:vars p)) 1)
                        (unfold-pis (rest (:vars p)) (:dom p) (:cod p))
                        (:cod p))]
              (ok> (parse benv (:dom p)) :as dom
                   (parse (cons (first (:vars p)) benv) cod) :as cod
                   (list 'Π dom cod)))
            ::arrow
            (ok> (parse benv (first (:doms p))) :as dom
                 (parse (cons '_ benv)
                        (if (> (count (:doms p)) 1)
                          (list* '-> (conj (into [] (rest (:doms p))) (:cod p)))
                          (:cod p))) :as cod
                 (list 'Π dom cod))))

     (annotation? term)
     (ok> (when-not (= (count term) 3)
            [:ko> "Annotation needs exactly two arguments." {:term term}])
          term :as [_ typ e]
          (parse benv typ) :as typ
          (parse benv e) :as e
          (list 'the typ e))

     (if? term)
     (ok> (parse-if term) :as [_ cnd x Tret tthen telse]
          (parse benv cnd) :as cnd
          (conj benv x) :as benv'
          (parse benv' Tret) :as Tret
          (parse benv tthen) :as tthen
          (parse benv telse) :as telse
          (list 'if [cnd Tret] tthen telse))
     
     (application? term)
     (ok> (when-not (>= (count term) 2)
            [:ko> "Application needs at least two operands." {:term term}])
          (parse benv (first term)) :as rator
          (if (seq (drop 2 term))
            (parse benv (rest term))
            (parse benv (second term))) :as rand
          (list rator rand))
     
     (variable? term)
     (if-let [lvl (search-boundvar benv term)]
       #{lvl}
       term)

     (pair? term)
     (ok> (if (or (> (count term) 2))
            (unfold-pair term)
            term) :as [A B]
          (parse benv A) :as fst
          [:ko> "Cannot parse term of pair" {:pair term :term A}]
          (parse benv B) :as snd
          [fst snd])

     (pair-type? term)
     (ok> (if (> (count term) 3)
            (unfold-pair-type (rest term))
            term) :as [_ A B]
          (parse-binding A) :as [v A]
          (parse benv A) :as fst
          [:ko> "Cannot parse first element of pair type" {:pair-type term, :term A}]
          (parse (cons v benv) B) :as scd
          [:ko> "Cannot parse second element of pair type" {:pair-type term, :term B}]
          (list 'Σ fst scd))
     
     (proj? term)
     (ok> (when-not (= (count term) 3)
            [:ko> "Projection needs exactly two argument. (proj i [a0 a1 ... aN])" {:term term}])
          term :as [_ i pair]
          (when-not (and (integer? i) (<= 0 i))
            [:ko> "Projection first argument must be a positive integer" {:term term}])
          (parse benv pair) :as p
          (unfold-proj i p))

     :else (throw (ex-info "Cannot parse term." {:term term})))))


(defn unfold-lambdas [params body]
  (if (seq params)
    (list 'λ [(first params)] (unfold-lambdas (rest params) body))
    body))

(example
 (unfold-lambdas '[x y z] 'x) => '(λ [x] (λ [y] (λ [z] x))))

(defn unfold-pis [vars dom cod]
  (if (seq vars)
    (list '-> [(first vars) dom] (unfold-pis (rest vars) dom cod))
    cod))

(example
 (unfold-pis '[x y z] 't 'u) => '(-> [x t] (-> [y t] (-> [z t] u))))

(defn unfold-pair [p]
  (if (> (count p) 2)
    [(first p) (unfold-pair (subvec p 1))]
    p))

(example
 (unfold-pair '[A B C D]) => '[A [B [C D]]])

(defn unfold-pair-type [p]
  (if (> (count p) 2)
    (list 'sig (first p) (unfold-pair-type (rest p)))
    (list* 'sig p)))

(example
 (unfold-pair-type '([a A] B C a))
 => '(sig [a A] (sig B (sig C a))))

(defn unfold-proj [i term]
  (if (zero? i)
    (list 'π1 term)
    (unfold-proj (dec i) (list 'π2 term))))

(example
 (unfold-proj 3 '[A [B [C [D nil]]]])
 => '(π1 (π2 (π2 (π2 [A [B [C [D nil]]]])))))

(defn search-boundvar [benv x]
  (loop [benv benv, lvl 0]
    (if (seq benv)
      (if (= (first benv) x)
        lvl
        (recur (rest benv) (inc lvl)))
      nil)))

(examples
 (search-boundvar '(x y z) 'x) => 0
 (search-boundvar '(x y z) 'z) => 2
 (search-boundvar '(x y z) 't) => nil)

(examples
 (parse '(lambda [x y z] x)) => '(λ (λ (λ #{2})))
 (parse '(lambda [x y z] z)) => '(λ (λ (λ #{0})))
 (parse '(lambda [x y z] t)) => '(λ (λ (λ t))))

(examples
 (parse '(-> [x A] x)) => '(Π A #{0})
 (parse '(-> [x y z A] x)) => '(Π A (Π A (Π A #{2})))
 (parse '(-> A B)) => '(Π A B)
 (parse '(-> A B C D)) => '(Π A (Π B (Π C D))))

(examples
 (parse '(x y)) => '(x y)
 (parse '(x y z)) => '(x (y z))
 (parse '(x y z t)) => '(x (y (z t)))
 (parse '(lambda [x y z] (x y z))) => '(λ (λ (λ (#{2} (#{1} #{0}))))))

(examples
 (parse '(-> [x :bool] (if [x :as z :return (x z)] x :bool)))
 => '(Π :bool (if [#{0} (#{1} #{0})] #{0} :bool))

 (parse '(-> [x :bool] (if [y :as x :return :type] x :bool)))
 => '(Π :bool (if [y :type] #{0} :bool))

 (parse '(the (-> :bool :bool) (lambda [x] x)))
 => '(the (Π :bool :bool) (λ #{0})))

(examples
 (parse '(sig [a A] [b a] C a))
 => '(Σ A (Σ #{0} (Σ C #{2})))

 (parse '(sig [a A] B C a))
 => '(Σ A (Σ B (Σ C #{2})))

 (parse '(sig (if :bla :bla) A))
 => '[:ko "Cannot parse first element of pair type" 
      {:pair-type (sig (if :bla :bla) A), 
       :term (if :bla :bla), 
       :cause [:ko> "Wrong arity for if expression, expecting 4." 
               {:term (if :bla :bla), 
                :arity 3}]}]

 (parse '[A B C D])
 => '[A [B [C D]]]

 (parse '(proj -1 [A  B C D]))
 => '[:ko> "Projection first argument must be a positive integer" {:term (proj -1 [A B C D])}]

 (parse '(proj 0 [A  B C D]))
 => '(π1 [A [B [C D]]]))

