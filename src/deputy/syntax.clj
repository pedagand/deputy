(ns deputy.syntax
  (:require
   [clojure.test :refer [is]]
   [clojure.set :as set]
   [clojure.string :as str]
   [deputy.presyntax :as pre]
   [deputy.utils :as u :refer [example examples do-for-example]]))

(def +examples-enabled+ true)

(defn reserved? [s]
  (contains? pre/reserved-symbols s))

;;; Generic matching forms
;;; ================

(defn atom? [at]
  (fn [t] (= t at)))

(defn constructor? [constr]
  (fn [t]
    (and (u/seq1? t)
         (= (first t) constr))))

(def operator? constructor?)

;;; canonical forms
;;; ============

;; ********************************

(def type-type?
  (atom? :type))

;; ********************************

(def pi-type?
  (constructor? 'Π))

(def lambda-term?
  (constructor? 'λ))

;; ********************************

(def bool-type?
  (atom? :bool))

(def true-term?
  (atom? 'true))

(def false-term?
  (atom? 'false))

;; ********************************

(def unit-type?
  (atom? :unit))

(def nil-term?
  (atom? nil))

;; ********************************

(def sigma-type?
  (constructor? 'Σ))

(defn pair-term? [t]
  (and (vector? t)
       (= (count t) 2)))

;; ********************************

(def label-type?
  (atom? :label))

(defn tag-term? [t]
  (keyword? t))

;; ********************************

(def labels-type?
  (atom? :labels))

(defn labels-term? [t]
  (and (list? t)
       (every? keyword? t)))

(examples
 (labels-term? '(x y)) => false
 (labels-term? '(:x :y)) => true)

;; ********************************

(def enum-type?
  (constructor? 'enum))

(def zero-term?
  (atom? 0))

(def suc-term?
  (constructor? 'suc))

;; ********************************

(def desc-type?
  (atom? :desc))

(def dunit-term?
  (atom? ":unit"))

(def dvar-term?
  (atom? "X"))

(def drecord-term?
  (constructor? "record"))

(def dsigma-term?
  (constructor? ":Σ"))

(def dpi-term?
  (constructor? ":Π"))


(def dunit2-term '(con2 [0 nil]))
(def dvar2-term '(con2 [(suc 0) nil]))
(defn dsigma2-term [S D] (list 'con2 ['(suc (suc 0)) [S D]]))
(defn dpi2-term [S D] (list 'con2 ['(suc (suc (suc 0))) [S D]]))

(def descD 
  (dsigma2-term
   '(enum [:unit [:X [:Σ [:Π nil]]]])
   (list 'λ (list 'caseD #{0}
             [dunit2-term
              [dunit2-term
               [(dsigma2-term :type (list 'λ (dpi2-term #{0} (list 'λ dvar2-term))))
                [(dsigma2-term :type (list 'λ (dpi2-term #{0} (list 'λ dvar2-term))))
                 nil]]]]))))

(def desc2-type (list 'μ2 descD))

(defn desc2-type? [t]
  (= t desc2-type))

(defn dunit2-term? [t]
  (= t dunit2-term))

(defn dvar2-term? [t]
  (= t dvar2-term))

(defn dsigma2-term? [t]
  (and (= (first t) 'con2)
       (let [[_ [nb [S D]]] t]
         (= nb '(suc (suc 0))))))

(example
 (dsigma2-term? (dsigma2-term 'S 'D)) => true)

(defn dpi2-term? [t]
  (and (= (first t) 'con2)
       (let [[_ [nb [S D]]] t]
         (= nb '(suc (suc (suc 0)))))))

(example
 (dpi2-term? (dpi2-term 'S 'D)) => true)

;; ********************************

(def mu-type?
  (constructor? 'μ))

(def mu2-type?
  (constructor? 'μ2))

(def con-term?
  (constructor? 'con))

(def con2-term?
  (constructor? 'con2))

;; ********************************

(def canonicals
  [type-type?
   pi-type? lambda-term?
   bool-type? true-term? false-term?
   unit-type? nil-term?
   sigma-type? pair-term?
   labels-type? labels-term?
   enum-type? zero-term? suc-term?
   desc-type? dunit-term? dvar-term? dsigma-term? dpi-term?
   mu-type? con-term?
   mu2-type? con2-term?
   label-type? tag-term? ])

(defn canonical-term? [t]
  (u/exists (fn [can?] (can? t)) canonicals))

(examples
 (canonical-term? :type) => true
 (canonical-term? '(Σ :type #{0})) => true
 (canonical-term? '(proj1 [:type :type])) => false)

;; ;;; elimination forms
;; ;;; =================

(defn freevar? [t]
  (and (symbol? t)
       (not (reserved? t))))

;; XXX: temporary representation ?
(defn boundvar? [t]
  (set? t))

;; ********************************

(defn application? [t]
  (and (u/seq1? t)
       (not (reserved? (first t)))))

(examples
 (application? '(x y)) => true
 (application? '(Π x y)) => false)

;; ********************************

(def annotation?
  (constructor? 'the))

(example
 (annotation? '(the T e)) => true)

;; ********************************

(def if?
  (constructor? 'if))

(example
 (if? '(if [t T] t1 t2)) => true)

;; ********************************

(def proj1?
  (operator? 'π1))

(def proj2?
  (operator? 'π2))

;; ********************************

(def record-type?
  (constructor? 'record))

(def case-term?
  (constructor? 'case))

(def caseD-term?
  (constructor? 'caseD))

;; ********************************

(def interp-term?
  (operator? 'interp))

(def interp2-term?
  (operator? 'interp2))

;; ********************************

(def neutrals
  [application?
   annotation?
   if?
   proj1? proj2?
   record-type? case-term? caseD-term?
   interp-term? interp2-term?])

(defn neutral-term? [t]
  (or (freevar? t)
      (boundvar? t)
      (u/exists (fn [can?] (can? t)) neutrals)))

;; ********************************

(def terms
  (concat canonicals neutrals))

(defn term? [t]
  (u/exists (fn [term?] (term? t)) terms))

;;; Assertions
;;; ========================

(defn freevars
  "Returns all names of variables with free occurrences in `t`."
  [t]
  (cond
    (freevar? t) #{t}
    (boundvar? t) #{}
    (type-type? t) #{}
    (pi-type? t)
    (let [[_ A B] t]
      (set/union (freevars A) (freevars B)))
    (lambda-term? t)
    (let [[_ B] t]
      (freevars B))
    (bool-type? t) #{}
    (true-term? t) #{}
    (false-term? t) #{}
    (unit-type? t) #{}
    (nil-term? t) #{}
    (sigma-type? t)
    (let [[_ A B] t]
      (set/union (freevars A) (freevars B)))
    (pair-term? t)
    (let [[t1 t2] t]
      (set/union (freevars t1) (freevars t2)))
    (labels-type? t) #{}
    (labels-term? t) #{}
    (enum-type? t)
    (let [[_ A] t]
      (freevars A))
    (zero-term? t) #{}
    (suc-term? t)
    (let [[_ A] t]
      (freevars A))
    (desc-type? t) #{}
    (dunit-term? t) #{}
    (dvar-term? t) #{}
    (dsigma-term? t)
    (let [[_ A B] t]
      (set/union (freevars A) (freevars B)))
    (dpi-term? t)
    (let [[_ A B] t]
      (set/union (freevars A) (freevars B)))
    (mu-type? t)
    (let [[_ A] t]
      (freevars A))
    (con-term? t)
    (let [[_ A] t]
      (freevars A))
    (mu2-type? t)
    (let [[_ A] t]
      (freevars A))
    (con2-term? t)
    (let [[_ A] t]
      (freevars A))
    (label-type? t) #{}
    (tag-term? t) #{}
    (application? t)
    (let [[A B] t]
      (set/union (freevars A) (freevars B)))
    (annotation? t)
    (let [[_ A B] t]
      (set/union (freevars A) (freevars B)))
    (if? t)
    (let [[_ [t T] t1 t2] t]
      (set/union (freevars t) (freevars T) (freevars t1) (freevars t2)))
    (proj1? t)
    (let [[_ A] t]
      (freevars A))
    (proj2? t)
    (let [[_ A] t]
      (freevars A))
    (record-type? t)
    (let [[_ A B] t]
      (set/union (freevars A) (freevars B)))
    (case-term? t)
    (let [[_ [t T] cs] t]
      (set/union (freevars t) (freevars T) (freevars cs)))
    (caseD-term? t)
    (let [[_ t cs] t]
      (set/union (freevars t) (freevars cs)))
    (interp-term? t)
    (let [[_ A B] t]
      (set/union (freevars A) (freevars B)))
    (interp2-term? t)
    (let [[_ A B] t]
      (set/union (freevars A) (freevars B)))))


(examples
 (freevars '(λ y))
 => '#{y})

(examples
 (freevars '(λ (#{0} y))) => '#{y}
 (freevars '(Π x (#{0} y))) => '#{x y}
 (freevars '(if [x y] u v)) => '#{x y u v}
 (freevars '[x y]) => '#{x y}
 (freevars '([x y] u)) => '#{x y u})

(defn locally-closed?
  "Checks that bound variables in `t` are well-scoped. It is only
  locally-closed since free variable may still occur."
  ([t] (locally-closed? t 0))
  ([idx t]
   (cond
     (boundvar? t) (< (first t) idx)
     (freevar? t) true
     (lambda-term? t)
     (let [[_ B] t]
       (locally-closed? (inc idx) B))
     (type-type? t) true
     (pi-type? t)
     (let [[_ A B] t]
       (and (locally-closed? idx A)
            (locally-closed? idx B)))
     (bool-type? t) true
     (true-term? t) true
     (false-term? t) true
     (unit-type? t) true
     (nil-term? t) true
     (sigma-type? t)
     (let [[_ A B] t]
       (and (locally-closed? idx A) (locally-closed? idx B)))
     (pair-term? t)
     (let [[A B] t]
       (and (locally-closed? idx A) (locally-closed? idx B)))
    (labels-type? t) true
    (labels-term? t) true
    (enum-type? t)
    (let [[_ A] t]
      (locally-closed? idx A))
    (zero-term? t) true
    (suc-term? t)
    (let [[_ A] t]
      (locally-closed? idx A))
    (desc-type? t) true
    (dunit-term? t) true
    (dvar-term? t) true
    (dsigma-term? t)
    (let [[_ A B] t]
      (and (locally-closed? idx A) (locally-closed? idx B)))
    (dpi-term? t)
    (let [[_ A B] t]
      (and (locally-closed? idx A) (locally-closed? idx B)))
    (mu-type? t)
    (let [[_ A] t]
      (locally-closed? idx A))
    (con-term? t)
    (let [[_ A] t]
      (locally-closed? idx A))
    (mu2-type? t)
    (let [[_ A] t]
      (locally-closed? idx A))
    (con2-term? t)
    (let [[_ A] t]
      (locally-closed? idx A))
    (label-type? t) true
    (tag-term? t) true
    (application? t)
    (let [[A B] t]
      (and (locally-closed? idx A) (locally-closed? idx B)))
    (annotation? t)
    (let [[_ A B] t]
      (and (locally-closed? idx A) (locally-closed? idx B)))
    (if? t)
    (let [[_ [t T] t1 t2] t]
      (and (locally-closed? idx t)
           (locally-closed? idx T)
           (locally-closed? idx t1)
           (locally-closed? idx t2)))
    (proj1? t)
    (let [[_ A] t]
      (locally-closed? idx A))
    (proj2? t)
    (let [[_ A] t]
      (locally-closed? idx A))
    (record-type? t)
    (let [[_ A B] t]
      (and (locally-closed? idx A)
           (locally-closed? idx B)))
    (case-term? t)
    (let [[_ [t T] cs] t]
      (and (locally-closed? idx t)
           (locally-closed? idx T)
           (locally-closed? idx cs)))
    (caseD-term? t)
    (let [[_ t cs] t]
      (and (locally-closed? idx t) (locally-closed? idx cs)))
    (interp-term? t)
    (let [[_ A B] t]
      (and (locally-closed? idx A) (locally-closed? idx B)))
    (interp2-term? t)
    (let [[_ A B] t]
      (and (locally-closed? idx A) (locally-closed? idx B))))))

(examples
 (locally-closed? '(interp "X" :bool))
 => true

 (locally-closed? '(interp ("record" [:x [:y [:z nil]]] [":unit" ["X" ["X" nil]]]) :type))
 => true)

(declare normal-form-canonical?
         normal-form-neutral?)

;; XXX: to check \eta-long form, we need type and its heavy

(defn normal-form?
  "Checks that `t` is a \beta-normal form."
  [t]
  {:pre  [(is (locally-closed? t)
              "Term must be locally-closed.")]}
  (normal-form-canonical? t))

(defn normal-form-canonical?
  [t]
  (cond
    (lambda-term? t)
    (let [[_ B] t]
      (normal-form-canonical? B))
    (type-type? t) true
    (pi-type? t)
    (let [[_ A B] t]
      (and (normal-form-canonical? A)
           (normal-form-canonical? B)))
    (bool-type? t) true
    (true-term? t) true
    (false-term? t) true
    (unit-type? t) true
    (nil-term? t) true
    (sigma-type? t)
    (let [[_ A B] t]
      (and (normal-form-canonical? A)
           (normal-form-canonical? B)))
    (pair-term? t)
    (let [[A B] t]
      (and (normal-form-canonical? A)
           (normal-form-canonical? B)))
    (labels-type? t) true
    (labels-term? t) true
    (enum-type? t)
    (let [[_ A] t]
      (normal-form-canonical? A))
    (zero-term? t) true
    (suc-term? t)
    (let [[_ A] t]
      (normal-form-canonical? A))
    (desc-type? t) true
    (dunit-term? t) true
    (dvar-term? t) true
    (dsigma-term? t)
    (let [[_ A B] t]
      (and (normal-form-canonical? A)
           (normal-form-canonical? B)))
    (dpi-term? t)
    (let [[_ A B] t]
      (and (normal-form-canonical? A) (normal-form-canonical? B)))
    (mu-type? t)
    (let [[_ A] t]
      (normal-form-canonical? A))
    (con-term? t)
    (let [[_ A] t]
      (normal-form-canonical? A))
    (mu2-type? t)
    (let [[_ A] t]
      (normal-form-canonical? A))
    (con2-term? t)
    (let [[_ A] t]
      (normal-form-canonical? A))
    (label-type? t) true
    (tag-term? t) true
    :else (normal-form-neutral? t)))

(defn normal-form-neutral?
  [t]
  (cond
    (freevar? t) true
    (boundvar? t) true
    (annotation? t) false

    (application? t)
    (let [[A B] t]
      (and (normal-form-neutral? A)
           (normal-form-canonical? B)))
    (if? t)
    (let [[_ [t T] t1 t2] t]
      (and (normal-form-neutral? t)
           (normal-form-canonical? T)
           (normal-form-canonical? t1)
           (normal-form-canonical? t2)))
    (proj1? t)
    (let [[_ A] t]
      (normal-form-neutral? A))
    (proj2? t)
    (let [[_ A] t]
      (normal-form-neutral? A))
    (record-type? t)
    (let [[_ A B] t]
      (and (normal-form-neutral? A)
           (normal-form-canonical? B)))
    (case-term? t)
    (let [[_ [t T] cs] t]
      (and (normal-form-neutral? t)
           (normal-form-canonical? T)
           (normal-form-canonical? cs)))
    (caseD-term? t)
    (let [[_ t cs] t]
      (and (normal-form-neutral? t)
           (normal-form-canonical? cs)))
    (interp-term? t)
    (let [[_ A B] t]
      (and (normal-form-neutral? A)
           (normal-form-canonical? B)))
    (interp2-term? t)
    (let [[_ A B] t]
      (and (normal-form-canonical? A)
           (normal-form-canonical? B)))
    :else false))


(examples
 (normal-form-neutral? '#{0}) => true
 (normal-form? '(λ (#{0} y))) => true
 (normal-form? '(λ #{0})) => true
 (normal-form? '((λ #{0}) y)) => false
 (normal-form? 'true) => true
 (normal-form? 'false) => true
 (normal-form? :type) => true
 (normal-form? :bool) => true
 (normal-form? '[false true]) => true
 (normal-form? '(λ ((λ #{0}) y))) => false)

;; ;; Substitutions
;; ;; =============


(defn subst
  "Substitutes in term `t` all occurrences of bound variable (i.e. `#{k}`) by
  (locally-closed) term `e` at level `k` (depth of binder)."
  ([e t]
   {:pre  [(is (locally-closed? 1 t)
               "Source term must be locally-closed above index 0")
           (is (locally-closed? e)
               "Substituted term must be locally-closed")]
;    :post [(is (set/subset? (freevars %) (set/union (freevars t) (freevars e)))
;               "Free variables are preserved by substitution")]
    }
   (subst e 0 t))
  ([e idx t]
   (cond
     (boundvar? t)
     (if (= (first t) idx)
       e
       #{(first t)})

     (freevar? t) t

     (type-type? t) t
     (pi-type? t)
     (let [[_ A B] t]
       (list 'Π (subst e idx A) (subst e idx  B)))
     (lambda-term? t)
     (let [[_ B] t]
       (list 'λ (subst e (inc idx) B)))
     (bool-type? t) t
     (true-term? t) t
     (false-term? t) t
     (unit-type? t) t
     (nil-term? t) t
     (sigma-type? t)
     (let [[_ A B] t]
       (list 'Σ (subst e idx  A) (subst e idx  B)))
     (pair-term? t)
     (let [[t1 t2] t]
       [(subst e idx  t1) (subst e idx t2)])
     (labels-type? t) t
     (labels-term? t) t
     (enum-type? t)
     (let [[_ A] t]
       (list 'enum (subst e idx  A)))
     (zero-term? t) t
     (suc-term? t)
     (let [[_ A] t]
       (list 'suc (subst e idx  A)))
     (desc-type? t) t
     (dunit-term? t) t
     (dvar-term? t) t
     (dsigma-term? t)
     (let [[_ A B] t]
       (list ":Σ" (subst e idx  A) (subst e idx  B)))
     (dpi-term? t)
     (let [[_ A B] t]
       (list ":Π" (subst e idx  A) (subst e idx  B)))
     (mu-type? t)
     (let [[_ A] t]
       (list 'μ (subst e idx  A)))
     (con-term? t)
     (let [[_ A] t]
       (list 'con (subst e idx  A)))
     (mu2-type? t)
     (let [[_ A] t]
       (list 'μ2 (subst e idx  A)))
     (con2-term? t)
     (let [[_ A] t]
       (list 'con2 (subst e idx  A)))
     (label-type? t) t
     (tag-term? t) t
     (application? t)
     (let [[A B] t]
       (list (subst e idx  A) (subst e idx  B)))
     (annotation? t)
     (let [[_ A B] t]
       (list 'the (subst e idx  A) (subst e idx  B)))
     (if? t)
     (let [[_ [t T] t1 t2] t]
      (list 'if [(subst e idx  t) (subst e idx  T)] (subst e idx  t1) (subst e idx  t2)))
     (proj1? t)
     (let [[_ A] t]
       (list 'π1 (subst e idx  A)))
     (proj2? t)
     (let [[_ A] t]
       (list 'π2 (subst e idx  A)))
     (record-type? t)
     (let [[_ A B] t]
       (list 'record (subst e idx  A) (subst e idx  B)))
     (case-term? t)
     (let [[_ [t T] cs] t]
       (list 'case [(subst e idx  t) (subst e idx  T)] (subst e idx  cs)))
     (caseD-term? t)
     (let [[_ t cs] t]
       (list 'caseD (subst e idx  t) (subst e idx  cs)))
     (interp-term? t)
     (let [[_ A B] t]
       (list 'interp (subst e idx  A) (subst e idx  B)))
     (interp2-term? t)
     (let [[_ A B] t]
       (list 'interp2 (subst e idx  A) (subst e idx  B))))))

(examples
 (subst '(λ (#{0} d)) '(λ ((#{1} #{0}) c)))
 => '(λ (((λ (#{0} d)) #{0}) c))
 
 (subst true -2 '(Σ :bool (λ (Σ #{1} (λ (Σ #{0} (λ (Σ #{1} :bool))))))))
 => '(Σ :bool (λ (Σ #{1} (λ (Σ true (λ (Σ true :bool)))))))

 (subst true '(π1 (Σ #{0} (λ #{1}))))
 => '(π1 (Σ true (λ true)))

 (subst false '(":Σ" #{0} (λ true)))
 => '(":Σ" false (λ true))

 (subst false '(case [#{0} (λ #{1})] #{0}))
 => '(case [false (λ false)] false)

 (subst true '(:x :y :z))
 => '(:x :y :z))

;; Discharge free variable
;; =============

(defn bind

  "Bind free variable `x` in term `t` to the nearest enclosing scope
  (ie., bind it to index `#{0}`)."

  ([x t]
   {:pre  [(is (locally-closed? t)
               "input term must be locally-closed")]
    :post [(is (not (contains? (freevars %) x))
               "input variable cannot be free in the resulting term")
           (is (locally-closed? 1 %)
               "resulting term is locally-closed above index 0")]}
   (bind x 0 t))
  ([x idx t]
   (cond
     (freevar? t) (if (= t x) #{idx} t)

     (boundvar? t) t

     (type-type? t) t
     (pi-type? t)
     (let [[_ A B] t]
       (list 'Π (bind x idx A) (bind x idx  B)))
     (lambda-term? t)
     (let [[_ B] t]
       (list 'λ (bind x (inc idx) B)))
     (bool-type? t) t
     (true-term? t) t
     (false-term? t) t
     (unit-type? t) t
     (nil-term? t) t
     (sigma-type? t)
     (let [[_ A B] t]
       (list 'Σ (bind x idx  A) (bind x idx  B)))
     (pair-term? t)
     (let [[t1 t2] t]
       [(bind x idx  t1) (bind x idx t2)])
     (labels-type? t) t
     (labels-term? t) t
     (enum-type? t)
     (let [[_ A] t]
       (list 'enum (bind x idx  A)))
     (zero-term? t) t
     (suc-term? t)
     (let [[_ A] t]
       (list 'suc (bind x idx  A)))
     (desc-type? t) t
     (dunit-term? t) t
     (dvar-term? t) t
     (dsigma-term? t)
     (let [[_ A B] t]
       (list ":Σ" (bind x idx  A) (bind x idx  B)))
     (dpi-term? t)
     (let [[_ A B] t]
       (list ":Π" (bind x idx  A) (bind x idx  B)))
     (mu-type? t)
     (let [[_ A] t]
       (list 'μ (bind x idx  A)))
     (con-term? t)
     (let [[_ A] t]
       (list 'con (bind x idx  A)))
     (mu2-type? t)
     (let [[_ A] t]
       (list 'μ2 (bind x idx  A)))
     (con2-term? t)
     (let [[_ A] t]
       (list 'con2 (bind x idx  A)))
     (label-type? t) t
     (tag-term? t) t
     (application? t)
     (let [[A B] t]
       (list (bind x idx  A) (bind x idx  B)))
     (annotation? t)
     (let [[_ A B] t]
       (list 'the (bind x idx  A) (bind x idx  B)))
     (if? t)
     (let [[_ [t T] t1 t2] t]
      (list 'if [(bind x idx  t) (bind x idx  T)] (bind x idx  t1) (bind x idx  t2)))
     (proj1? t)
     (let [[_ A] t]
       (list 'π1 (bind x idx  A)))
     (proj2? t)
     (let [[_ A] t]
       (list 'π2 (bind x idx  A)))
     (record-type? t)
     (let [[_ A B] t]
       (list 'record (bind x idx  A) (bind x idx  B)))
     (case-term? t)
     (let [[_ [t T] cs] t]
       (list 'case [(bind x idx  t) (bind x idx  T)] (bind x idx  cs)))
     (caseD-term? t)
     (let [[_ t cs] t]
       (list 'caseD (bind x idx  t) (bind x idx  cs)))
     (interp-term? t)
     (let [[_ A B] t]
       (list 'interp (bind x idx  A) (bind x idx  B)))
     (interp2-term? t)
     (let [[_ A B] t]
       (list 'interp2 (bind x idx  A) (bind x idx  B))))))

(example
 (bind 'x12 '(λ (π1 (Σ x12 #{0}))))
 => '(λ (π1 (Σ #{1} #{0}))))

(example
 (let [x (gensym "x")]
   (bind 'x '(λ (π1 (Σ x #{0})))))
 => '(λ (π1 (Σ #{1} #{0}))))

(example
 (bind 'x '(:x :y :z)) => '(:x :y :z))
