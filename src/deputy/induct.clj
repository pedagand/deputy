(ns deputy.induct
  (:require [deputy.utils :as u :refer [example examples do-for-example ok>]]
            [deputy.presyntax :as prestx :refer [parse]]
            [deputy.syntax :as stx]))

(def +examples-enabled+ true)

(defn desc-one? [d]
  (= d ::1))

(example
 (desc-one? ::1) => true)

(defn desc-x? [d]
  (= d ::x))

(example
 (desc-x? ::x) => true)

(defn desc-pi? [d]
  (and (u/seq1? d)
       (= (first d) ::Π)))

(defn interpretation [d]
  (cond 
    (desc-one? d) (fn [x] []) ;; unit is [] of type :unit
    (desc-x? d) (fn [x] x)
    ;; Pi-code
    (desc-pi? d) 
    (let [[_ S T] d
          fun (parse (list 'Π ['s S] (interpretation (T 's))))]
      (fn [x] (fun x)))
    :else (throw (ex-info "Unsupported descriptor." {:descriptor d}))))
