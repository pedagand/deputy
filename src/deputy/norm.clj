(ns deputy.norm
  
  "Normalization by evaluation."

  (:require [clojure.test :refer [is]]
            [clojure.set :as set]
            [deputy.utils :as u :refer [example examples ok>]]
            [deputy.syntax :as stx]))

(def +examples-enabled+ true)

;;; host values
;;; ======

(defn vlambda? [t]
  (fn? t))

(defn vtrue? [t]
  (= t true))

(defn vfalse? [t]
  (= t false))

(defn vnil? [t]
  (= t nil))

;; XXX: use Clojure's vectors to represent pairs for uniformity with
;; the above. *But* this means that we have to disambiguate with the
;; rest of the syntax. Ugly?
;; Note: I keep struggling with bugs coming from this. FIXME!

(def reserved-kwd 
  #{::vpi,::vsigma,::vapp,::vif,::vproj1,::vproj2,
    ::vfree,::vsuc,::venum,::vrecord,::vcase,::vcaseD,
    ::vdsigma, ::vdpi,
    ::vinterp, ::vinterp2,
    ::vmu, ::vcon
    ::vmu2, ::vcon2})

(defn vpair? [t]
  (and (vector? t)
       (= (count t) 2)
       (not (contains? reserved-kwd (first t)))))

(example
 (vpair? '[::vfree x]) => false)

;;; values

(defn vtype? [t]
  (stx/type-type? t))

(defn vbool? [t]
  (stx/bool-type? t))

(defn vunit? [t]
  (stx/unit-type? t))

(defn vlabel? [t]
  (stx/label-type? t))

(defn vtag? [t]
  (stx/tag-term? t))

(defn vlabels? [t]
  (stx/labels-type? t))

(defn vpi? [t]
  (and (u/vector1? t)
       (= (first t) ::vpi)))

(defn vsigma? [t]
    (and (u/vector1? t)
         (= (first t) ::vsigma)))

(defn venum? [t]
  (and (u/vector1? t)
       (= (first t) ::venum)))

(defn vzero? [t]
  (stx/zero-term? t))

(defn vsuc? [t]
  (and (u/vector1? t)
       (= (first t) ::vsuc)))

(defn vdesc? [t]
  (stx/desc-type? t))

(defn vdunit? [t]
  (stx/dunit-term? t))

(defn vdvar? [t]
  (stx/dvar-term? t))

(defn vdsigma? [t]
  (and (u/vector1? t)
       (= (first t) ::vdsigma)))

(defn vdpi? [t]
  (and (u/vector1? t)
       (= (first t) ::vdpi)))

(defn vmu? [t]
  (and (u/vector1? t)
       (= (first t) ::vmu)))

(defn vcon? [t]
  (and (u/vector1? t)
       (= (first t) ::vcon)))

;;; bootstrap constructions

(declare vdesc2)

(defn vmu2? [t]
  (and (u/vector1? t)
       (= (first t) ::vmu2)))

(defn vcon2? [t]
  (and (u/vector1? t)
       (= (first t) ::vcon2)))

(defn vnum2? [t n]
  (if (zero? n)
    (vzero? t)
    (and (vsuc? t)
         (vnum2? (second t) (dec n)))))

(defn vdunit2? [t]
  (and (vcon2? t)
       (let [[_ [nb _]] t]
         (vnum2? nb 0))))

(defn vdvar2? [t]
  (and (vcon2? t)
       (let [[_ [nb _]] t]
         (vnum2? nb 1))))

(defn vdsigma2? [t]
  (and (vcon2? t)
       (let [[_ [nb _]] t]
         (vnum2? nb 2))))

(defn vdpi2? [t]
  (and (vcon2? t)
       (let [[_ [nb _]] t]
         (vnum2? nb 3))))

;;; neutrals
;;; ======

(defn vfree? [t]
  (and (u/vector1? t)
       (= (first t) ::vfree)))

(defn vapp? [t]
  (and (u/vector1? t)
       (= (first t) ::vapp)))

(defn vif? [t]
  (and (u/vector1? t)
       (= (first t) ::vif)))

(defn vproj1? [t]
    (and (u/vector1? t)
         (= (first t) ::vproj1)))

(defn vproj2? [t]
    (and (u/vector1? t)
         (= (first t) ::vproj2)))

(defn vrecord? [t]
  (and (u/vector1? t)
       (= (first t) ::vrecord)))

(defn vcase? [t]
  (and (u/vector1? t)
       (= (first t) ::vcase)))

(defn vcaseD? [t]
  (and (u/vector1? t)
       (= (first t) ::vcaseD)))

(defn vinterp? [t]
  (and (u/vector1? t)
       (= (first t) ::vinterp)))

(defn vinterp2? [t]
  (and (u/vector1? t)
       (= (first t) ::vinterp2)))


(defn vneutral? [t]
  (or (vfree? t)
      (vapp? t)
      (vif? t)
      (vproj1? t)
      (vproj2? t)
      (vrecord? t)
      (vcase? t)
      (vcaseD? t)
      (vinterp? t)
      (vinterp2? t)))

;;; Evaluation
;;; ==========

(defn env-push
  "Push value `v` in evaluation `env`ironment."
  [env v]
  (conj env v))

(defn env-fetch
  "Fetch `n`-th value in `env`ironment."
  [env n]
  (nth env (- (count env) (inc n)))
  ;;(nth env n)
)

(example
 (-> []
     (env-push 1)
     (env-push 2)
     (env-push 3)
     (env-fetch 0)) => 3)

(defn right-nest [l]
  (if (empty? l) 
    nil
    [(first l) (right-nest (rest l))]))

(declare vapp vif vproj1 vproj2 vrecord vcase vcaseD vinterp vinterp2 vlabels)

(defn evaluation
  "Evaluate `term` in environment `env` by piggy-backing on Clojure's β-reduction."
  ([term]
   {:pre [(stx/locally-closed? term)]}
   (evaluation [] term))
  ([env term]
  ;; XXX: test always failing, I don't think that `keys` does what I want
  ;; {:pre [(is (set/subset? (stx/freevars term) (keys env))
  ;;            "Free variables must be defined in the environment")]}
   (cond
    ;; canonical terms
    (stx/type-type? term) :type

    (stx/pi-type? term)
    (let [[_ A B] term]
      [::vpi (evaluation env A) (evaluation env B)])

    (stx/lambda-term? term)
    (let [[_ t] term]
      ;; XXX: can we precompile the term `t` so that occurrences of
      ;;      x are directly bound to Clojure's `fn` parameter ?
      (fn [y] (evaluation (env-push env y) t)))


    (stx/bool-type? term) :bool

    (stx/true-term? term) true

    (stx/false-term? term) false

    (stx/unit-type? term) :unit

    (stx/nil-term? term) nil

    (stx/sigma-type? term)
    (let [[_ A B] term]
      [::vsigma (evaluation env A) (evaluation env B)])

    (stx/pair-term? term)
    (let [[t1 t2] term]
      [(evaluation env t1) (evaluation env t2)])

    (stx/label-type? term) term
    (stx/tag-term? term) term

    (stx/labels-type? term) term

    (stx/enum-type? term)
    (let [[_ l] term]
      (cond 
        ;; HACK: make `type-check` produce a value instead
        (and (list? l)
             (every? keyword? l))
        [::venum (right-nest l)]

        :else
        [::venum (evaluation env l)]))

    (stx/zero-term? term) term
    (stx/suc-term? term)
    (let [[_ t] term]
      [::vsuc (evaluation env t)])

    (stx/desc-type? term) :desc

    (stx/dunit-term? term) ":unit"
    (stx/dvar-term? term) "X"

    (stx/dsigma-term? term)
    (let [[_ A B] term]
      [::vdsigma (evaluation env A) (evaluation env B)])

    (stx/dpi-term? term)
    (let [[_ A B] term]
      [::vdpi (evaluation env A) (evaluation env B)])

    (stx/mu-type? term)
    (let [[_ D] term]
      [::vmu (evaluation env D)])

    (stx/con-term? term)
    (let [[_ xs] term]
      [::vcon (evaluation env xs)])

    ;; bootstrap variants

    (stx/mu2-type? term)
    (let [[_ D] term]
      [::vmu2 (evaluation env D)])

    (stx/con2-term? term)
    (let [[_ xs] term]
      [::vcon2 (evaluation env xs)])

    ;; computation terms

    (stx/boundvar? term)
    (env-fetch env (first term))

    (stx/freevar? term) [::vfree term]

    (stx/application? term)
    (let [[e t] term]
      (vapp (evaluation env e) (evaluation env t)))

    (stx/if? term)
    (let [[_ [t T] t1 t2] term]
      (vif (evaluation env t)
           (evaluation env T)
           (evaluation env t1)
           (evaluation env t2)))

    (stx/proj1? term)
    (let [[_ t] term]
      (vproj1 (evaluation env t)))

    (stx/proj2? term)
    (let [[_ t] term]
      (vproj2 (evaluation env t)))

    (stx/record-type? term)
    (let [[_ l T] term]
      (let [l' (cond 
                 ;; HACK: make `type-check` produce a value instead
                 (and (list? l)
                      (every? keyword? l))
                 (right-nest l)

                 :else (evaluation env l))]

        (vrecord l' (evaluation env T))))

    (stx/case-term? term)
    (let [[_ [e T] cs] term]
      (vcase (evaluation env e)
             (evaluation env T)
             (evaluation env cs)))

    (stx/caseD-term? term)
    (let [[_ e cs] term]
      (vcaseD (evaluation env e)
              (evaluation env cs)))

    (stx/interp-term? term)
    (let [[_ D X] term]
      (vinterp (evaluation env D) (evaluation env X)))

    ;; bootstrap version
    (stx/interp2-term? term)
    (let [[_ D X] term]
      (vinterp2 (evaluation env D) (evaluation env X)))

    ;; annotations are computationally transparent

    (stx/annotation? term)
    (let [[_ _ t] term]
      (evaluation env t))

    :else
    (throw (ex-info "Cannot evaluation: unsupported term." {:term term})))))

(def vdesc2 (evaluation stx/desc2-type))

(defn vapp
  [rator rand]
  {:pre [(or (fn? rator)
             (vneutral? rator))]}
  (cond
    (fn? rator) (rator rand)

    :else [::vapp rator rand]))

(defn vif
  [nt vT v1 v2]
  {:pre [(or (vtrue? nt)
             (vfalse? nt)
             (vneutral? nt))]}
  (cond
    (vtrue? nt) v1
    (vfalse? nt) v2

    :else [::vif [nt vT] v1 v2]))

(defn vproj1
  [nt]
  {:pre [(or (vpair? nt)
             (vneutral? nt))]}
  (cond
    (vpair? nt) (first nt)

    ::else [::vproj1 nt]))

(defn vproj2
  [nt]
  {:pre [(or (vpair? nt)
             (vneutral? nt))]}
  (cond
    (vpair? nt) (second nt)

    ::else [::vproj2 nt]))

(defn vrecord
  [ne T]
  {:pre [(or (vnil? ne)
             (vpair? ne)
             (vneutral? ne))
         (fn? T)]}
  (cond
    (vnil? ne) :unit
    (vpair? ne)
    (let [[_ n] ne]
      [::vsigma (T 0) (fn [_] (vrecord n (fn [x] (T [::vsuc x]))))])

    ::else [::vrecord ne T]))

(defn vcase
  [ne T cs]
  {:pre [(or (vzero? ne)
             (vsuc? ne)
             (vneutral? ne))]}
  (cond
    (vzero? ne) (first cs)
    (vsuc? ne)
    (let [[_ n] ne]
      (vcase n (fn [x] (T [::vsuc x])) (second cs)))

    ::else [::vcase ne T cs]))

(defn vcaseD
  [ne cs]
  {:pre [(or (vzero? ne)
             (vsuc? ne)
             (vneutral? ne))]}
  (cond
    (vzero? ne) (first cs)
    (vsuc? ne)
    (let [[_ n] ne]
      (vcaseD n (second cs)))

    ::else [::vcaseD ne cs]))

(defn vinterp
  [ne X]
  {:pre [(or (vdunit? ne)
             (vdvar? ne)
             (vdsigma? ne)
             (vdpi? ne)
             (vneutral? ne))]}
  (cond
    (vdunit? ne) :unit
    (vdvar? ne) X

    (vdsigma? ne)
    (let [[_ vS vD] ne]
      [::vsigma vS (fn [v] (vinterp (vapp vD v) X))])

    (vdpi? ne)
    (let [[_ vS vD] ne]
      [::vpi vS (fn [v] (vinterp (vapp vD v) X))])

    ::else [::vinterp ne X]))

(defn vinterp2
  [ne X]
  {:pre [(or (vdunit2? ne)
             (vdvar2? ne)
             (vdsigma2? ne)
             (vdpi2? ne)
             (vneutral? ne))]}
  (cond
    (vdunit2? ne) :unit
    (vdvar2? ne) X

    (vdsigma2? ne)
    (let [[_ [_ [vS vD]]] ne]
      [::vsigma vS (fn [v] (vinterp2 (vapp vD v) X))])

    (vdpi2? ne)
    (let [[_ [_ [vS vD]]] ne]
      [::vpi vS (fn [v] (vinterp2 (vapp vD v) X))])

    ::else [::vinterp2 ne X]))


(examples
 (evaluation '(the :bool true)) => true

 (evaluation '(π1 ((λ #{0}) [:type :bool]))) => :type

 (evaluation '(record nil (λ :type))) => :unit

 (evaluation '(case [0 (λ :type)] [:unit nil])) => :unit
 (evaluation '(case [0 (λ :type)] [:bool nil])) => :bool
 (evaluation '(case [0 (λ :type)] [:bool [:unit [:bool nil]]])) => :bool
 (evaluation '(case [(suc 0) (λ :type)] [:bool [:unit [:bool nil]]])) => :unit
 (evaluation '(case [(suc (suc 0)) (λ :type)] [:bool [:unit [:bool nil]]])) => :bool
 ;; XXX: add example where it is stuck
 ;; XXX: add example where types evolve

 (evaluation '(interp ":unit" :type)) => :unit
 (evaluation '(interp "X" :type)) => :type
 (evaluation '(interp "X" :bool)) => :bool
 
 ;; XXX: stick to checking WHNF because of higher-order representation
 (first (evaluation '(interp (":Σ" :type (λ "X")) :type))) => ::vsigma

  (evaluation  {'t :label
                'l :labels} '(enum [t l]))
  => '[::venum [[::vfree t] [::vfree l]]]

  (evaluation  {} '(enum (:x :y :z)))
  => '[::venum [:x [:y [:z nil]]]]

  ;; bootstrap versions

 (vdunit2? (evaluation stx/dunit2-term)) => true
 (vdvar2? (evaluation stx/dvar2-term)) => true
 (vdsigma2? (evaluation (stx/dsigma2-term 'S 'D))) => true
 (vdpi2? (evaluation (stx/dpi2-term 'S 'D))) => true

 (evaluation (list 'interp2 stx/dunit2-term :type)) => :unit
 (evaluation (list 'interp2 stx/dvar2-term :type)) => :type
 (evaluation (list 'interp2 stx/dvar2-term :bool)) => :bool
 
 ;; XXX: stick to checking WHNF because of higher-order representation
 (first (evaluation (list 'interp2 (stx/dsigma2-term :type (list 'λ stx/dvar2-term)) :type))) => ::vsigma

)



;;; Reification
;;; ===========

(declare quot-check quot-synth)

(defn quot
  "Quote the (locally-closed) `value` (of type `vtype`) to obtain the corresponding term (in normal form)."
  ([vtype value] 
   {:pre [(stx/locally-closed? value)]}
   (quot {} vtype value))
  ([ctx vtype value]
   {:post [(is (stx/normal-form? %) "`quot` must produce a β-normal form.")
           ;; XXX: also, the result must be well-typed!
           ]}
   (quot-check ctx vtype value)))

(defn quot-check
  [ctx vtype value]
  (cond
    ;; η-rules
    (vunit? vtype) nil

    (vpi? vtype)
    (let [[_ vA vB] vtype
          f value
          x (gensym "__quot")
          v [::vfree x]
          env' (assoc ctx x vA)]
      (list 'λ (stx/bind x (quot-check env' (vB v) (vapp f v)))))

    (vsigma? vtype)
    (let [[_ vA vB] vtype
          v1 (vproj1 value)]
      [(quot-check ctx vA v1) (quot-check ctx (vB v1) (vproj2 value))])

    ;; XXX: bit of a hack to enable overloading of pairs for labels
    
    (and (vlabels? vtype)
         (vpair? value))
         [(quot-check ctx :label (first value))
          (quot-check ctx :labels (second value))]

    (and (vlabels? vtype)
         (vnil? value))
    nil

    ;; structural rules

    (vtype? value) :type

    (vunit? value) :unit

    (vpi? value)
    (let [[_ vA vB] value]
      (list 'Π
            (quot-check ctx :type vA)
            (quot-check ctx [::vpi vA (fn [_] :type)] vB)))

    (vlambda? value)
    (do (assert (vpi? vtype))
        ;; IMPOSSIBLE CASE
        (assert false))

    (vsigma? value)
    (let [[_ vA vB] value]
      (list 'Σ
            (quot-check ctx :type vA)
            (quot-check ctx [::vpi vA (fn [_] :type)] vB)))

    (vpair? value)
    (do (assert (vsigma? vtype))
        (let [[_ vA vB] vtype]
          [(quot-check ctx vA (first value))
           (quot-check ctx vB (second value))]))

    (vbool? value) :bool
    (vtrue? value) true
    (vfalse? value) false

    (vbool? value) :bool
    (vtrue? value) true
    (vfalse? value) false

    (vlabels? value) :labels

    (vlabel? value) :label
    (vtag? value) value

    (venum? value)
    (let [[_ l] value]
      (list 'enum (quot-check ctx :labels l)))

    (vzero? value) value
    (vsuc? value) 
    (let [[_ n] value
          [_ l] vtype]
      (assert (vpair? l))
      (let [[_ l2] l]
        (list 'suc 
              (quot-check ctx [::venum l2] n))))

    (vdesc? value) :desc

    (vdunit? value) ":unit"
    (vdvar? value) "X"

    (vdsigma? value)
    (let [[_ vS vD] value]
      (list ":Σ"
            (quot-check ctx :type vS)
            (quot-check ctx [::vpi vS (fn [_] :desc)] vD)))

    (vdpi? value)
    (let [[_ vS vD] value]
      (list ":Π" 
            (quot-check ctx :type vS)
            (quot-check ctx [::vpi vS (fn [_] :desc)] vD)))

    (vmu? value)
    (let [[_ vD] value]
      (list 'μ (quot-check ctx :desc vD)))

    (vcon? value)
    (let [[_ vxs] value
          [_ vD] vtype]
      (assert (vmu? vtype))
      (list 'con (quot-check ctx (vinterp vD vtype) vxs)))

    (vmu2? value)
    (let [[_ vD] value]
      (list 'μ2 (quot-check ctx vdesc2 vD)))

    (vcon2? value)
    (let [[_ vxs] value
          [_ vD] vtype]
      (assert (vmu2? vtype))
      (list 'con2 (quot-check ctx (vinterp2 vD vtype) vxs)))

    :else (first (quot-synth ctx value))))

(defn quot-synth
  [ctx value]
  {:pre [(vneutral? value)]
   :post [;; Returns a pair
          (vector? %)
          (= (count %) 2)
          ;; Whose first component is a normal form
          (stx/normal-form? (first %))
          ;; XXX: Whose second component is a type
          ;; XXX: also, the result must be well-typed!
          ]}
  (cond
    (vfree? value)
    (let [[_ var] value
          vtype (get ctx var)]
      [var vtype])

    (vapp? value)
    (let [[_ neu val] value
          [f vtype] (quot-synth ctx neu)]
      (assert (vpi? vtype))
      (let [[_ vA vB] vtype]
        (assert (fn? vB))
        [(list f (quot-check ctx vA val))
         (vB val)]))

    (vif? value)
    (let [[_ [neu vB] v1 v2] value]
      [(list 'if
             [(quot-check ctx :bool neu)
              (quot-check ctx [::vpi :bool (fn [_] :type)] vB)]
             (quot-check ctx (vB true) v1)
             (quot-check ctx (vB false) v2))
       (vB neu)])

    (vproj1? value)
    (let [[_ neu] value
          [n vAB] (quot-synth ctx neu)]
      (assert (vsigma? vAB))
      (let [[_ vA vB] vAB]
        [(list 'π1 n) vA]))

    (vproj2? value)
    (let [[_ neu] value
          [n vAB] (quot-synth ctx neu)]
      (assert (vsigma? vAB))
      (let [[_ vA vB] vAB]
        [(list 'π2 n) (vB (vproj1 neu))]))

    (vrecord? value)
    (let [[_ neu vT] value
          [n vE] (quot-synth ctx neu)]
      [(list 'record n
             (quot-check ctx [::vpi [::venum vE] (fn [_] :type)] vT))
       :type])

    (vcase? value)
    (let [[_ neu vT vcs] value
          [n vE] (quot-synth ctx neu)]
      (assert (venum? vE))
      
      (let [T (quot-check ctx [::vpi vE (fn [_] :type)] vT)
            [_ vl] vE
            cs (quot-check ctx (vrecord vl vT) vcs)]
        [(list 'case [n T] cs) (vT neu)]))

    (vcaseD? value)
    (let [[_ neu vcs] value
          [n vE] (quot-synth ctx neu)]
      (assert (venum? vE))

      (let [[_ vl] vE
           cs (quot-check ctx (vrecord vl (fn [_] vdesc2)) vcs)]
        [(list 'caseD n cs) vdesc2]))

    (vinterp? value)
    (let [[_ nD vX] value
          [D _] (quot-synth ctx nD)]
      [(list 'interp D (quot-check ctx :type vX)) :type])

    ;; bootstrap version
    (vinterp2? value)
    (let [[_ nD vX] value
          [D _] (quot-synth ctx nD)]
      [(list 'interp2 D (quot-check ctx :type vX)) :type])

    :else
    (throw (ex-info "Unsupported value." {:value value}))))

(examples
 (quot-synth '{x :bool} '[::vfree x])
 => ['x :bool]
 (quot-synth '{x :bool
               y :type} '[::vfree x])
 => ['x :bool]
 (quot-synth '{x :bool
               y :type} '[::vfree y])
 => ['y :type]
 (quot-check '{x :bool} :bool '[::vfree x])
 => 'x)

(examples
 (quot-synth '{x [::venum [:x [:y [:z nil]]]]} [::vrecord [::vfree 'x] (fn [x] :type)])
 => '[(record x (λ :type)) :type]
 (quot-synth {'T [::vpi [::venum [:x [:y [:z nil]]]] (fn [x] :type)]
              'x [::venum [:x [:y [:z nil]]]]} [::vrecord [::vfree 'x] (fn [v] [::vapp [::vfree 'T] v])])
 => '[(record x (λ (T #{0}))) :type]
 (quot-synth {'ls :labels
              'T [::vpi [::venum [::vfree 'ls]] (fn [x] :type)]
              'x [::venum [::vfree 'ls]]}
             [::vrecord [::vfree 'x] (fn [v] [::vapp [::vfree 'T] v])])
 => '[(record x (λ (T #{0}))) :type]

 (quot-check {'ls :labels
              'T [::vpi [::venum [::vfree 'ls]] (fn [x] :type)]
              'x [::venum [::vfree 'ls]]}
             :type
             [::vrecord [::vfree 'x] (fn [v] [::vapp [::vfree 'T] v])])
 => '(record x (λ (T #{0})))
 (quot-check {'T [::vpi [::venum nil] (fn [_] :type)]}
             :type
             (evaluation '(record nil (λ (T #{0})))))
 => :unit
 (quot-check {'T [::vpi [::venum [:x [:y [:z nil]]]] (fn [x] :type)]}
             :type
             (evaluation '(record [:x [:y [:z nil]]] (λ (T #{0})))))
 => '(Σ (T 0) 
        (λ (Σ (T (suc 0)) 
              (λ (Σ (T (suc (suc 0))) 
                    (λ :unit))))))
)

(examples
 (quot-synth '{x [::venum [:x [:y [:z nil]]]]} 
             [::vcase [::vfree 'x] (fn [x] :type) [:unit [:unit [:unit nil]]]])
 => '[(case [x (λ :type)] [:unit [:unit [:unit nil]]]) :type]
 (quot-synth {'T [::vpi [::venum [:x [:y [:z nil]]]] (fn [x] :type)]
              'x [::venum [:x [:y [:z nil]]]]}
             [::vcase [::vfree 'x] 
              (fn [v] [::vapp [::vfree 'T] v])
              [:unit [:bool [:bool nil]]]])
 => '[(case [x (λ (T #{0}))] 
        [:unit [:bool [:bool nil]]]) [::vapp [::vfree T] [::vfree x]]])

(examples
 (quot-check {} :desc ":unit")
 => ":unit"
 (quot-check {} :desc "X")
 => "X"
 (quot-check {} :desc [::vdsigma :type (fn [x] ":unit")])
 => '(":Σ" :type (λ ":unit"))
 (quot-check {} :desc [::vdpi :bool (fn [x] (vif x (fn [_] :desc) ":unit" "X"))])
 => '(":Π" :bool (λ (if [#{0} (λ :desc)] ":unit" "X")))
 (quot-check {} :desc (evaluation '(":Π" :bool (λ (if [#{0} (λ :desc)] ":unit" "X")))))
 => '(":Π" :bool (λ (if [#{0} (λ :desc)] ":unit" "X")))

 (quot-check {} :type (evaluation '(interp  ":unit" :type)))
 => :unit
 (quot-check {} :type (evaluation '(interp  "X" :bool)))
 => :bool
 (quot-check {} :type (evaluation '(interp (":Π" :bool (λ (if [#{0} (λ :desc)] ":unit" "X"))) :type)))
 => '(Π :bool (λ (interp (if [#{0} (λ :desc)] ":unit" "X") :type)))
 (quot-check {} :type (evaluation '(interp (":Σ" :bool (λ (if [#{0} (λ :desc)] ":unit" "X"))) :type)))
 => '(Σ :bool (λ (interp (if [#{0} (λ :desc)] ":unit" "X") :type))))



;; bootstrap version
(examples

 (quot-check {} vdesc2 (evaluation stx/descD))
 => stx/descD

 (quot-check {} :type vdesc2)
 => stx/desc2-type

 (quot-check {} :type (evaluation (list 'interp2 stx/dunit2-term :type))) 
 => :unit

 (quot-check {} :type (evaluation (list 'interp2 stx/dvar2-term :bool))) 
 => :bool

 (quot-check {} :type (evaluation (list 'interp2 (stx/dpi2-term :bool (list 'λ (list 'if [#{0} (list 'λ stx/desc2-type)] stx/dunit2-term stx/dvar2-term))) :type)))
 => (list 'Π :bool (list 'λ (list 'interp2 (list 'if [#{0} (list 'λ stx/desc2-type)] stx/dunit2-term stx/dvar2-term) :type)))

 (quot-check {} :type (evaluation (list 'interp2 (stx/dsigma2-term :bool (list 'λ (list 'if [#{0} (list 'λ stx/desc2-type)] stx/dunit2-term stx/dvar2-term))) :type)))
 => (list 'Σ :bool (list 'λ (list 'interp2 (list 'if [#{0} (list 'λ stx/desc2-type)] stx/dunit2-term stx/dvar2-term) :type)))

 (quot-check {} vdesc2 (evaluation stx/dunit2-term))
 => stx/dunit2-term

 (quot-check {} vdesc2 (evaluation stx/dvar2-term))
 => stx/dvar2-term

 (quot-check {} vdesc2 (evaluation (stx/dsigma2-term :type (list 'λ stx/dunit2-term))))
 => (stx/dsigma2-term :type (list 'λ stx/dunit2-term))

 (quot-check {} vdesc2 (evaluation (stx/dpi2-term :bool (list 'λ (list 'if [#{0} (list 'λ stx/desc2-type)] stx/dunit2-term stx/dvar2-term)))))
 => (stx/dpi2-term :bool (list 'λ (list 'if [#{0} (list 'λ stx/desc2-type)] stx/dunit2-term stx/dvar2-term)))

 )

;;; Normalization (by evaluation)
;;; =============================

(defn normalize
  ([vtype term]
   (normalize {} vtype term))
  ([ctx vtype term]
   {:pre [(stx/locally-closed? term)]
    :post [(stx/normal-form? %)]
    }
   (quot ctx vtype (evaluation [] term))))

;; Test η-expansions

(examples
 (normalize :unit 'NOT-NEEDED)
 => nil

 (normalize [::vpi
              [::vpi :type (fn [x] :type)]
              (fn [y] [::vpi :type (fn [x] :type)])] '(λ #{0}))
 => '(λ (λ (#{1} #{0})))

 (normalize [::vpi
              [::vsigma :type (fn [x] :type)]
              (fn [y] [::vsigma :type (fn [x] :type)])] '(λ #{0}))
 => '(λ [(π1 #{0}) (π2 #{0})]))

;; Test canonical terms

(examples
 ;; :type
 (normalize :type :type)
 => :type
 (normalize :type '((λ #{0}) :type))
 => :type
 (normalize :type '(((λ (λ #{1})) :type) :bool))
 => :type

 ;; Π / Σ
 (normalize :type '(Π :bool (λ (if [#{0} (λ :type)] :bool :type))))
 => '(Π :bool (λ (if [#{0} (λ :type)] :bool :type)))
 (normalize :type '(Σ :bool (λ (if [#{0} (λ :type)] :bool :type))))
 => '(Σ :bool (λ (if [#{0} (λ :type)] :bool :type)))

 ;; λ
 (normalize [::vpi :bool (fn [x] :bool)] '(λ #{0}))
 => '(λ #{0})
 (normalize [::vpi :bool (fn [x] [::vpi :bool (fn [y] :bool)])] '(λ (λ #{0})))
 => '(λ (λ #{0}))
 (normalize [::vpi :bool (fn [x] [::vpi :bool (fn [y] :bool)])] '(λ (λ #{1})))
 => '(λ (λ #{1}))
 (normalize [::vpi [::vsigma :bool (fn [x] :type)]
                   (fn [x] [::vsigma :bool (fn [y] :type)])]
            ;; swap . swap = id
            '((λ (λ (#{1} (#{1} #{0})))) (λ [(π2 #{0}) (π1 #{0})])))
 => '(λ [(π1 #{0}) (π2 #{0})])


 ;; pair
 (normalize [::vsigma :bool (fn [x] (if x :unit :bool))] [true 'NOT-NEEDED])
 => '[true nil]
 (normalize [::vsigma :bool (fn [x] (if x :unit :bool))] [false true])
 => '[false true]

 ;; bool
 (normalize :type :bool)
 => :bool
 (normalize :type '(((λ (λ #{0})) :type) :bool))
 => :bool

 ;; true / false
 (normalize :bool true)
 => true
 (normalize :bool false)
 => false
 (normalize :bool '((if [((λ #{0}) false) (λ (→ :unit :bool))] (λ false) (λ true)) nil))
 => true

 ;; desc & fixpoints
 (normalize :desc '(":Σ"
                    :type
                    (λ ":unit")))
 => '(":Σ" :type (λ ":unit"))

 (normalize :desc '(":Σ"
                    (enum (:Z :S))
                    (λ ":unit")))
 => '(":Σ" (enum [:Z [:S nil]]) (λ ":unit"))

 (normalize :desc '(":Σ"
                    (enum (:Z :S))
                    (λ (case [#{0} (λ :desc)]
                         [":unit" ["X" nil]]))))
 => '(":Σ" (enum [:Z [:S nil]]) (λ (case [#{0} (λ :desc)] [":unit" ["X" nil]])))

 (normalize :type '(μ
                      (":Σ"
                       (enum (:Z :S))
                       (λ (case [#{0} (λ :desc)]
                            [":unit" ["X" nil]])))))
 => '(μ (":Σ" (enum [:Z [:S nil]]) (λ (case [#{0} (λ :desc)] [":unit" ["X" nil]])))))

;; Neutral terms

(examples

 ;; free var
 (normalize {'x :bool} :bool 'x)
 => 'x

 ;; if
 (normalize {'x [::vpi :unit (fn [x] :bool)]} :type
            '(if [(x NOT-NEEDED) (λ :type)] :bool :unit))
 => '(if [(x nil) (λ :type)] :bool :unit)

 ;; π1 / π2
 (normalize {'x [::vsigma :bool (fn [x] :bool)]} :bool
            '(π1 x))
 => '(π1 x)
 (normalize {'x [::vsigma :bool (fn [x] :bool)]} :bool
            '(π2 x))
 => '(π2 x)

 ;; η-rules
 (normalize {'x [::vsigma :bool (fn [x] :bool)]} [::vsigma :bool (fn [x] :bool)] 'x)
 => '[(π1 x) (π2 x)]
 (normalize {'x [::vsigma [::vsigma :bool (fn [y] :bool)] (fn [x] :bool)]}
            [::vsigma [::vsigma :bool (fn [y] :bool)] (fn [x] :bool)] 'x)
 => '[[(π1 (π1 x)) (π2 (π1 x))] (π2 x)]
  (normalize {'x [::vsigma :bool (fn [x] :unit)]} [::vsigma :bool (fn [x] :unit)] 'x)
 => '[(π1 x) nil]
 (normalize {'x [::vpi :bool (fn [x] :unit)]} [::vpi :bool (fn [x] :unit)] 'x)
 => '(λ nil)
 (normalize {'x [::vpi :bool (fn [x] :bool)]} [::vpi :bool (fn [x] :bool)] 'x)
 => '(λ (x #{0}))
 (normalize {'x [::vpi :unit (fn [x] :bool)]} :bool '(x NOT-NEEDED))
 => '(x nil)
 (normalize {'x [::vpi :bool (fn [x] :unit)]} :unit '(x false))
 => nil)

